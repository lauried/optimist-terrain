# Optimist Terrain Generation

## About

This is a library for generating heightmap terrain.
It's still a work in progress but it can produce results without crashing, it
passes its own tests, and I'm fairly happy with the interface.

[See BUILD.md for building instructions.](BUILD.md)

## Overview

Optimist Terrain Generation creates terrain procedurally and deterministically.
The terrain is represented both by a heightmap and a "texture", which is just
an abstract way to categorise the surface type at a point on the terrain.

To control how terrain is generated, the library parses a language which allows
the user to define at a fairly high level how terrain is created from scaled
and filtered noise and processed. Each instance of code in this language is
known as a terrain type, as its intended purpose is to define different types
of terrain - for example, rolling hills, grassland, swamp, rocky mountains.

An endless expanse of a single terrain type would be pretty boring, so the user
is able to define a map of terrain types themselves at a much larger scale, and
the library will blend between those types to produce the final terrain. The
map can be on a much larger scale: the output heightmap can use 1-metre
resolution, while the map can specify terrain types and coarse heights with
32-metre resolution or higher as desired.

A quick glossary:

- **Tile**: An individual heightmap sample, or the space between two samples.
- **Texture**: An abstraction for surface properties that the library doesn't care about.
- **Chunk**: A rectangle of generated tiles.
- **Terrain Type**: One 'script' in the language.
- **Map**: The high level, large-scale map of what terrain types go where.

## Interface

The interface is defined in [source/terraingen.h](source/terraingen.h).

### opti_registerTerrainType

Registers a new terrain type.

Parameters:

- **grammar** (const char*) A string describing how the terrain is generated. See Appendix A.

Returns:

- (int) The terrain type handle created. Terrain type handles may be reused if existing terrain types are freed.
- `OPTI_TERRAIN_GRAMMAR_ERROR` if there was an error parsing the grammar. The cause of a grammar error can be inspected with `opti_getLastError`.

### opti_updateTerrainType

Loads a new grammar into a previously registered terrain type.

Parameters:

- **grammar** (const char*) A string describing how the terrain is generated. See Appendix A.
- **terrain_type** (int) The terrain type handle to update.

Returns:

- (int) The terrain type handle that was given as the terrain_type parameter.
- `OPTI_TERRAIN_GRAMMAR_ERROR` if there was an error parsing the grammar. The cause of a grammar error can be inspected with `opti_getLastError`.
- `OPTI_TERRAIN_GRAMMAR_NON_EXISTENT` if the value passed to terrain_type was not a registered terrain type handle, i.e. it had not been returned from opti_registerTerrainType or it had been freed.

### opti_getLastError

Returns:

- (opti_TerrainGrammarError*) A struct representing the error. The property `line` (int) is the line in the code on which the error occurred, and the property `message` (char\[]) is a null terminated string describing the error.

### opti_freeTerrainType

Parameters:

- **terrain_type** (int) A terrain type as returned from `opti_registerTerrainType`.

Returns:

- `OPTI_TERRAIN_GRAMMAR_NON_EXISTENT` if the value passed to terrain_type was not a registered terrain type handle, i.e. it had not been returned from opti_registerTerrainType or it had been freed.

### opti_generateSingleTerrain

Generates a chunk of a single type of terrain.

Parameters:

- **terrain_type** (int) A terrain type as returned from `opti_registerTerrainType`.
- **x** (int) The minimum x coordinate of the chunk, in tile coordinates.
- **y** (int) The minimum y coordinate of the chunk, in tile coordinates.
- **w** (int) The x size of the chunk, in tiles.
- **h** (int) The y size of the chunk, in tiles.
- **lod** (int) Level of detail. Must be a power of 2. See Appendix on LOD.
- **heights** (float*) Pointer to an array where the heights will be written. The user must allocate space for `w * h` floats.
- **textures** (int*) Pointer to an array where the textures will be written. The user must allocate space for `w * h` ints.

Returns:

- `OPTI_TERRAIN_SUCCESS` if the terrain was successfully generated.
- `OPTI_TERRAIN_GRAMMAR_NON_EXISTENT` if the terrain type did not exist.
- An `OPTI_TERRAIN_INVALID_*` value if any of the parameters was not valid.

### opti_MapCallback

A function pointer type which the library will call to retrieve sections of the map.

Parameters:

- **user_data** (void*) Whatever was passed into `opti_generateMultiTerrain`.
- **x** (int) The minimum x coordinate of the chunk, in map coordinates.
- **y** (int) The minimum y coordinate of the chunk, in map coordinates.
- **w** (int) The x size of the chunk, in map coordinates.
- **h** (int) The y size of the chunk, in map coordinates.
- **terrain_types** (int*) A pointer to an array with room for `w * h` terrain types. The implementation must write the terrain types here.
- **coarse_heights** (float*) A pointer to an array with room for `w * h` heights. The implementation must write the heights here.

### opti_generateMultiTerrain

Generates a chunk of blended terrain, using a map of heightmaps.

Parameters:

- **callback** (opti_MapCallback) A user defined function for generating the map. The user implementation might use this library to generate the map.
- **user_data** (void*) This will be passed to `callback` when it is called, allowing the user to identify specific instances of generation.
- **map_scale** (int) The size of a map cell in tiles, i.e. the number of tiles between different terrain types.
- **x** (int) The minimum x coordinate of the chunk, in tile coordinates.
- **y** (int) The minimum y coordinate of the chunk, in tile coordinates.
- **w** (int) The x size of the chunk, in tiles.
- **h** (int) The y size of the chunk, in tiles.
- **lod** (int) Level of detail. Must be a power of 2, and map_scale/lod must be greater than or equal to 2. See Appendix on LOD.
- **heights** (float*) Pointer to an array where the heights will be written. The user must allocate space for `w * h` floats.
- **textures** (int*) Pointer to an array where the textures will be written. The user must allocate space for `w * h` ints.

Returns:

- `OPTI_TERRAIN_SUCCESS` if the terrain was successfully generated.
- An `OPTI_TERRAIN_INVALID_*` value if any of the parameters was not valid.

## Appendix A: Level Of Detail

The value used for LOD is a kind of scale factor used in skipping rows and
columns when generating terrain. This means that 1 is full detail, and higher
values have less detail. For technical reasons the value given for LOD must be
a power of 2.

When using a LOD that isn't 1, the library will generate enough heights and
textures to fill the given width and height, but importantly, the x and y
coordinates should stay the same regardless of LOD. They'll be truncated down
to a multiple of the LOD value automatically.

## Appendix B: Terrain Generation Grammar

Blank lines are ignored, lines beginning with # are comments, all others are
taken as instructions. The loader will not accept invalid instructions.

Instructions are executed in the order they are given in the script. They
operate on a stack of buffers, with the output buffer at the very bottom. A
buffer is the rectangle of heights and textures being generated.

### Commands

### buffer

`buffer new`

Creates a new buffer stacked on top of the current. The library should warn if
the instructions end with more than the initial buffer.

`buffer duplicate`

Creates a new buffer stacked on top of the current. The buffer created is a
duplicate of the current buffer.

`buffer merge add`

Merges the current buffer down by adding it to the one below. If there is
no buffer below then the code should halt and show an error.

`buffer merge sub`

Merges the current buffer down by subtracting it fron the one below. If
there is no buffer below then the code should halt and show an error.

`buffer merge mul`

Merges the current buffer down by multiplying it by the one below. The
result is scaled down by the top buffer's maximum possible range of values,
so that the range of the buffer below is not changed.

`buffer discard`

Discards the top buffer and returns to using the one below. This is useful
if we want to create a side buffer for texture or object placement but have
no use for it in the terrain height.

### terrain

`terrain noise <size> <height> <seed>`

Adds noise to the current buffer. Size is the feature size, height is the
vertical scale applied to the noise and seed chooses the set of noise to
use.

Size must be a positive integer power of 2.

Height is a floating point in final altitude coordinates, to scale the
noise from a range -1.0 - 1.0 up to the desired size.

Seed must be an integer.

`terrain shape <value> <value> <value> ...`

Shapes the current buffer. The values represent equally placed points on a
graph of input to output heights. The values are equally spaced for input,
and specify the output height at that position.

Shape values are floating point scale factors, representing evenly spaced
  points on a graph mapping input values to output values.

### texture

The texture command takes several optional modifiers.

`texture <texture> [ <condition> ... ]`

Without conditions, the command will apply the given texture to the entire
terrain. Subsequent textures will overwrite previous ones so a texture command
without conditions can be used to set the default texture.

Note that textures can be applied at any point in the script, allowing
for placement to be based off an intermediate heightmap rather than the
final one. Combined with the buffer command, this makes it possible to
render separate buffers purely for placing textures.

#### conditions for texture command

`probability <probability> <size> <seed>`

Generates noise at the size and seed and then places where the normalized
height of the new field is less than the probability value.

Probability is a floating point from `0.0` to `1.0`.

Size must be an integer power of 2.

Seed must be an integer.

`gradient <minimum> to <maximum>`

Applies the texture when the gradient is between the given values. `0.0` is
flat and `1.0` is vertical. Being a heightmap, vertical gradients are
impossible. The gradient is calculated by subtracting the normal z component
from `1.0`.

`gradient (above|below) <limit>`

`gradient below <limit>`

Applies the texture when the gradient is above or below the given value.

`altitude <minimum> to <maximum>`

Applies the texture in the given altitude range. The altitude is the value
that would be output if generation finished at this point in the script, for
a single terrain type. Coarse heights from the map will not have been applied
at this point.

`altitude (above|below) <limit>`

Applies the texture when the gradient is above or below the given value.

