/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "terrain/ProceduralTerrainType.hh"
#include <assert.h>
#include <math.h>
#include <stdio.h>
// C++ system
#include <algorithm>
#include <fstream>
#include <iostream>
#include <limits>
#include <string>
#include "library/containers/Array2D.hh"
#include "library/maths/Maths.hh"
#include "library/maths/IntegerMaths.hh"
#include "library/text/String.hh"
#include "library/text/TextParser.hh"
#include "terrain/NoiseSample.hh"
//#include "TerrainTexture.h"
//#include "StringUtil.h"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Constructor, destructor
//-----------------------------------------------------------------------------

ProceduralTerrainType::ProceduralTerrainType()
{
	is_loaded     = false;
	uses_gradient = false;
}

ProceduralTerrainType::~ProceduralTerrainType()
{
	hClearData();
}

void ProceduralTerrainType::hClearData()
{
	UnLoad();
}

//-----------------------------------------------------------------------------
// Helpers
//-----------------------------------------------------------------------------

void ProceduralTerrainType::hAddUniqueNoiseCount(int size, int seed)
{
	for (auto it = unique_noise_counts.begin(); it != unique_noise_counts.end(); ++it)
	{
		if ((*it).size == size && (*it).seed == seed)
		{
			++((*it).count);
			return;
		}
	}

	UniqueNoise  sample;
	sample.size  = size;
	sample.seed  = seed;
	sample.count = 1;
	unique_noise_counts.push_front(sample);
}

void ProceduralTerrainType::hSortUniqueNoise()
{
	unique_noise_counts.sort(ProceduralTerrainType::hCompareUniqueNoise);
}

bool ProceduralTerrainType::hCompareUniqueNoise(UniqueNoise &a, UniqueNoise &b)
{
	return a.count > b.count;
}

//-----------------------------------------------------------------------------
// Load from file
//-----------------------------------------------------------------------------

void ProceduralTerrainType::UnLoad()
{
	// These don't use pointers to objects and therefore can just be cleared
	// without an iterative delete.
	instructions.clear();
	unique_noise_counts.clear();
	
	is_loaded     = false;
	uses_gradient = false;
}

void ProceduralTerrainType::LoadData(const char *data, size_t dataSize)
{
	// Clear old data
	if (is_loaded)
	{
		is_loaded = false;
		hClearData();
	}
	num_buffers = 0;
	max_buffers = 0;

	// Set up a parser
	WhitespaceTokenizer tokenizer;
	TextParser parser(data, dataSize, tokenizer);

	while (!parser.Eof())
	{
		TextParser line = parser.GetLine();
		// Allow blank lines
		if (line.Eof())
		{
			continue;
		}

		std::string command = line.GetToken();

		// Allow lines starting with #
		if (command[0] == '#')
		{
			continue;
		}

		if (!command.compare("buffer"))
		{
			Instruction::InstructionType instructionType;

			std::string bufferCommand = line.GetToken();
			if (!bufferCommand.compare("new"))
			{
				++num_buffers;
				if (num_buffers > max_buffers)
				{
					max_buffers = num_buffers;
				}

				instructionType = Instruction::BUFFER_NEW;
			}
			else if (!bufferCommand.compare("duplicate"))
			{
				++num_buffers;
				if (num_buffers > max_buffers)
				{
					max_buffers = num_buffers;
				}

				instructionType = Instruction::BUFFER_DUPLICATE;
			}
			else if (!bufferCommand.compare("merge"))
			{
				--num_buffers;
				if (num_buffers < 0)
				{
					line.ThrowCustomParseException("Buffer merge causes buffer underflow");
				}

				std::string mergeType = line.GetToken();
				if (!mergeType.compare("add"))
				{
					instructionType = Instruction::BUFFER_MERGE_ADD;
				}
				else if (!mergeType.compare("sub"))
				{
					instructionType = Instruction::BUFFER_MERGE_SUB;
				}
				else if (!mergeType.compare("mul"))
				{
					instructionType = Instruction::BUFFER_MERGE_MUL;
				}
				else
				{
					line.ThrowCustomParseException("Unrecognized token; expected add, sub or mul");
				}
			}
			else if (!bufferCommand.compare("discard"))
			{
				--num_buffers;
				if (num_buffers < 0)
				{
					line.ThrowCustomParseException("Buffer discard causes buffer underflow");
				}

				instructionType = Instruction::BUFFER_DISCARD;
			}
			else
			{
				line.ThrowCustomParseException("Unrecognized token; expected new, duplicate, merge or discard");
			}

			Instruction instruction;
			instruction.type = instructionType;
			instruction.line_number = line.GetCurrentLineNumber();
			instructions.push_back(instruction);

			line.ExpectEof();
		}
		else if (!command.compare("terrain"))
		{
			std::string terrainOption = line.GetToken();
			if (!terrainOption.compare("noise"))
			{
				int size = String::ToInt(line.GetToken());
				float height = String::ToFloat(line.GetToken());
				int seed = String::ToInt(line.GetToken());

				// TODO: we should warn if parameters are the wrong format, like alpha in numeric values etc.

				if (!IntegerMaths::IsPowerOfTwo(size))
				{
					line.ThrowCustomParseException("terrain noise <size> is not a power of two");
				}

				hAddUniqueNoiseCount(size, seed);

				Instruction instruction;
				instruction.type = Instruction::TERRAIN_NOISE;
				instruction.line_number = line.GetCurrentLineNumber();
				instruction.data.noise.size = size;
				instruction.data.noise.height = height;
				instruction.data.noise.seed = seed;
				instructions.push_back(instruction);

				line.ExpectEof();
			}
			else if (!terrainOption.compare("shape"))
			{
				Instruction instruction;
				instruction.type = Instruction::TERRAIN_SHAPE;
				instruction.line_number = line.GetCurrentLineNumber();
				instruction.data.shape.num_values = 0;

				for (int i = 0; !line.Eof(); i += 1)
				{
					if (i >= kMaxShapeValues)
					{
						line.ThrowCustomParseException("Too many tokens after terrain noise");
					}

					instruction.data.shape.values[i] = String::ToFloat(line.GetToken());
					++(instruction.data.shape.num_values);
				}
				if (instruction.data.shape.num_values < 3)
				{
					line.ThrowCustomParseException("Expected at least 3 tokens after terrain noise");
				}
				instructions.push_back(instruction);
			}
		}
		else if (!command.compare("texture"))
		{
			int textureIndex = String::ToInt(line.GetToken());

			float probability  = 1.0f;
			int   prob_size    = 2;
			int   prob_seed    = 0;
			float gradient_min = 0.0f;
			float gradient_max = 0.0f;
			float altitude_min = 0.0f;
			float altitude_max = 0.0f;

			//if (line.Eof())
			//	line.ThrowCustomParseException("Expected tokens after texture");

			while (!line.Eof())
			{
				std::string textureOption = line.GetToken();
				if (!textureOption.compare("probability"))
				{
					probability = String::ToFloat(line.GetToken());
					prob_size = String::ToInt(line.GetToken());
					prob_seed = String::ToInt(line.GetToken());

					if (!IntegerMaths::IsPowerOfTwo(prob_size))
					{
						line.ThrowCustomParseException("Texture probability <prob> <size> <seed>; size must be a power of 2");
					}
				}
				else if (!textureOption.compare("gradient"))
				{
					std::string tok = line.GetToken();
					if (!tok.compare("above"))
					{
						gradient_min = String::ToFloat(line.GetToken());
						gradient_max = std::numeric_limits<float>::max();
					}
					else if (!tok.compare("below"))
					{
						gradient_min = 0;
						gradient_max = String::ToFloat(line.GetToken());
					}
					else
					{
						gradient_min = String::ToFloat(tok);
						line.ExpectToken("to");
						gradient_max = String::ToFloat(line.GetToken());
					}
				}
				else if (!textureOption.compare("altitude"))
				{
					std::string tok = line.GetToken();
					if (!tok.compare("above"))
					{
						altitude_min = String::ToFloat(line.GetToken());
						altitude_max = std::numeric_limits<float>::max();
					}
					else if (!tok.compare("below"))
					{
						altitude_min = 0;
						altitude_max = String::ToFloat(line.GetToken());
					}
					else
					{
						altitude_min = String::ToFloat(tok);
						line.ExpectToken("to");
						altitude_max = String::ToFloat(line.GetToken());
					}
				}

				Instruction instruction;
				instruction.type = Instruction::TEXTURE;
				instruction.line_number = line.GetCurrentLineNumber();
				instruction.data.texture_object.index = textureIndex;
				instruction.data.texture_object.probability  = probability;
				instruction.data.texture_object.prob_size    = prob_size;
				instruction.data.texture_object.prob_seed    = prob_seed;
				instruction.data.texture_object.gradient_min = gradient_min;
				instruction.data.texture_object.gradient_max = gradient_max;
				instruction.data.texture_object.altitude_min = altitude_min;
				instruction.data.texture_object.altitude_max = altitude_max;
				instructions.push_back(instruction);

				if (gradient_max != gradient_min)
				{
					uses_gradient = true;
				}
			}
		}
		else
		{
			line.ThrowCustomParseException("Unrecognized token; expected buffer, terrain or texture");
		}
	}

	if (num_buffers > 0)
	{
		parser.ThrowCustomParseException("Not all buffers were merged down");
	}

	is_loaded = true;
	hSortUniqueNoise();
}

//-----------------------------------------------------------------------------
// Show the loaded data
//-----------------------------------------------------------------------------

void ProceduralTerrainType::PrintInstructions()
{
	std::cout << "Instructions:" << std::endl;
	int buffer_count = 0;
	for (auto it = instructions.begin(); it != instructions.end(); ++it)
	{
		// TODO: display the parameters to make sure they're here ok
		std::cout << (*it).line_number << ": ";
		switch ((*it).type)
		{
		case Instruction::BUFFER_NEW:
			++buffer_count;
			std::cout << "BUFFER_NEW - " << buffer_count << " buffers" << std::endl;
			break;
		case Instruction::BUFFER_DUPLICATE:
			++buffer_count;
			std::cout << "BUFFER_DUPLICATE - " << buffer_count << " buffers" << std::endl;
			break;
		case Instruction::BUFFER_MERGE_ADD:
			--buffer_count;
			std::cout << "BUFFER_MERGE_ADD - " << buffer_count << " buffers" << std::endl;
			break;
		case Instruction::BUFFER_MERGE_SUB:
			--buffer_count;
			std::cout << "BUFFER_MERGE_SUB - " << buffer_count << " buffers" << std::endl;
			break;
		case Instruction::BUFFER_MERGE_MUL:
			--buffer_count;
			std::cout << "BUFFER_MERGE_MUL - " << buffer_count << " buffers" << std::endl;
			break;
		case Instruction::BUFFER_DISCARD:
			--buffer_count;
			std::cout << "BUFFER_DISCARD - " << buffer_count << " buffers" << std::endl;
			break;
		case Instruction::TERRAIN_NOISE:
			std::cout << "TERRAIN_NOISE " << (*it).data.noise.size << " " << (*it).data.noise.height << " " << (*it).data.noise.seed << std::endl;
			break;
		case Instruction::TERRAIN_SHAPE:
			std::cout << "TERRAIN_SHAPE" << std::endl;
			break;
		case Instruction::TEXTURE:
			std::cout << "TEXTURE" << std::endl;
			break;
		case Instruction::OBJECT:
			std::cout << "OBJECT" << std::endl;
			break;
		default:
			std::cout << "ERROR: Unknown type" << std::endl;
			break;
		}
	}
	std::cout << buffer_count << " buffers remain" << std::endl;
}

void ProceduralTerrainType::PrintNoiseList()
{
	std::cout << "Unique noise/seed list:" << std::endl;
	for (auto it = unique_noise_counts.begin(); it != unique_noise_counts.end(); ++it)
	{
		std::cout << (*it).count << ": size " << (*it).size << ", seed " << (*it).seed << std::endl;
	}
}

//-----------------------------------------------------------------------------
// Shape terrain
//-----------------------------------------------------------------------------

// Shapes the specified buffer. The values represent equally placed points on a
// graph of input to output heights. The shape values are equally spaced for
// input, and specify the output height at that position as a fraction of the
// current range of data.
// Min and max specify the minimum and maximum possible values for the data.
// It's important to use the possible values from generated noise so that all
// chunks of terrain are affected exactly the same.
static void ShapeTerrain(float *data, int data_size, float min, float max, float *shape, int num_values)
{
	// TODO: might be worth bounds checking values if there isn't too much
	// slowdown from it.

	if (max == min)
	{
		return;
	}

	float range = max - min;
	float input_step = range / (float)(num_values - 1);

	float shape_steps[num_values];
	for (int i=0; i<num_values - 1; ++i)
	{
		shape_steps[i] = shape[i+1] - shape[i];
	}
	shape_steps[num_values - 1] = 0.0f;

	for (int i=0; i<data_size; ++i)
	{
		float pos_in_range = data[i] - min;
		int index = pos_in_range / input_step;
		// TODO: This overflows when data[i] = max. Either keep clamping or fudge the values so this doesn't happen.
		if (index >= num_values)
		{
			index = num_values - 1;
		}
		float fraction = (pos_in_range - ((float)index*input_step)) / input_step;
		data[i] = min + (shape[index] + shape_steps[index]*fraction) * range;
	}
}

static void ShapeMinMax(float *min, float *max, float *shape, int num_values)
{
	float shape_min = std::numeric_limits<float>::max();
	float shape_max = std::numeric_limits<float>::min();

	for (int i=0; i<num_values; ++i)
	{
		if (shape[i] < shape_min)
		{
			shape_min = shape[i];
		}
		if (shape[i] > shape_max)
		{
			shape_max = shape[i];
		}
	}

	float range = *max - *min;
	*max = *min + range * shape_max;
	*min = *min + range * shape_min;
}

//-----------------------------------------------------------------------------
// Make terrain gradients
//-----------------------------------------------------------------------------

// Calculates the gradients of the points on the heightmap. The gradients
// buffer must already exist.
static void CalculateGradients(float *heights, int width, int height, float tileSize, float *gradients)
{
	if (width < 2 || height < 2)
	{
		return;
	}

	const Array2DWrapper<float> heightArray(heights, width, height);
	Array2DWrapper<float> gradientArray(gradients, width, height);

	for (int y = 0; y < height - 1; y += 1)
	{
		for (int x = 0; x < width - 1; x += 1)
		{
			float curr = heightArray.at(x, y);
			float nx = heightArray.at(x + 1, y) - curr;
			float ny = heightArray.at(x, y + 1) - curr;
			float nz = 1;
			float scale = 1.0f / sqrt(nx * nx + ny * ny + nz * nz);
			gradientArray.at(x, y) = 1.0f - (nz * scale);
		}
	}

	for (int y = 0, x = width - 1; y < height - 1; y += 1)
	{
		gradientArray.at(x, y) = gradientArray.at(x - 1, y);
	}
	for (int x = 0, y = height - 1; x < width - 1; x += 1)
	{
		gradientArray.at(x, y) = gradientArray.at(x, y - 1);
	}
	gradientArray.at(width - 1, height - 1) = gradientArray.at(width - 2, height - 2);
}

//-----------------------------------------------------------------------------
// Build terrain chunk using the instructions
//-----------------------------------------------------------------------------

void ProceduralTerrainType::BuildChunk(ProceduralTerrainTypeBuildParameters *chunk, float *buffer, int buffer_size)
{
	const float MINIMUM_FINAL_SCALE = 1.0f;

	// WARNING:
	// Nothing that changes during Chunk generation should ever go in
	// the TerrainType struct, for thread safety.

	// TODO: precalculate in advance as many noise types that will fit into
	// buffer, without any height scaling.
	// Even better optimization would avoid reserving a place in the buffer for
	// a particular noise except when it has to be used.
	// So if we use one type of noise twice and then another twice, we only
	// need one slot in the buffer.

	// Using this buffer as cache introduces a lot of possible optimizations
	// which could get a little complex. Also note that at the same frequency,
	// often a different seed is used each time so that different components of
	// terrain appear different.

	//-------------------------------------------------------------------------
	// asserts
	assert(Maths::IsPowerOfTwo(chunk->scale));
	assert(chunk->width  >= 1);
	assert(chunk->height >= 1);

	//-------------------------------------------------------------------------
	// set up values constant for the whole chunk's generation
	float one_over_scale = 1.0f / (float)chunk->scale;

	//-------------------------------------------------------------------------
	// set up the height buffer stack

	// TODO: Replace float x[variable] with vector or Array2D.

	// TODO: Each buffer manage its own data when we create the stack of them,
	// and maybe see if we can use an actual stack which will catch underflows.

	struct BufferInfo {
		float  range_min;
		float  range_max;
		float *data;
	};

	int   chunk_size = chunk->width*chunk->height;
	float stack_data[chunk_size*max_buffers];
	BufferInfo stack[max_buffers+1];
	int   current_buffer = 0;
	
	// TODO: Make this optional. Maybe the user had already reset the memory.
	std::fill(chunk->terrain, chunk->terrain + chunk_size, 0.0f);
	std::fill(chunk->textures, chunk->textures + chunk_size, 0);

	stack[0].data = chunk->terrain;
	for (int i=0; i<max_buffers; ++i)
	{
		stack[i+1].data = &stack_data[chunk_size*i];
	}
	stack[0].range_min = 0.0f;
	stack[0].range_max = 0.0f;

	//-------------------------------------------------------------------------
	// set up the gradient & probability buffers
	int gradient_buffer_size = (uses_gradient) ? chunk_size : 0;
	std::vector<float> gradient_buffer(gradient_buffer_size);

	int probability_buffer_size = chunk_size;
	std::vector<float> probability_buffer(probability_buffer_size);

	//-------------------------------------------------------------------------
	// iterate instructions in order
	for (auto it = instructions.begin(); it != instructions.end(); ++it)
	{
		Instruction *instruction = &(*it);

		// perform the instruction
		float final_scale;
		float one_over_final_scale;
		float inv_range;
		switch (instruction->type)
		{
		//---------------------------------------------------------------------
		case Instruction::BUFFER_NEW:
		//---------------------------------------------------------------------
			++current_buffer;
			if (current_buffer > max_buffers)
			{
				std::cout << "TerrainType::BuildChunk internal error: more buffers used than space allocated" << std::endl;
				return;
			}
			stack[current_buffer].range_min = 0.0f;
			stack[current_buffer].range_max = 0.0f;
			for (int i=0; i<chunk_size; ++i)
			{
				stack[current_buffer].data[i] = 0.0f;
			}
			break;
		//---------------------------------------------------------------------
		case Instruction::BUFFER_DUPLICATE:
		//---------------------------------------------------------------------
			++current_buffer;
			if (current_buffer > max_buffers)
			{
				std::cout << "TerrainType::BuildChunk internal error: more buffers used than space allocated" << std::endl;
				return;
			}
			stack[current_buffer].range_min = stack[current_buffer-1].range_min;
			stack[current_buffer].range_max = stack[current_buffer-1].range_max;
			for (int i=0; i<chunk_size; ++i)
			{
				stack[current_buffer].data[i] = stack[current_buffer-1].data[i];
			}
			break;
		//---------------------------------------------------------------------
		case Instruction::BUFFER_MERGE_ADD:
		//---------------------------------------------------------------------
			--current_buffer;
			if (current_buffer < 0)
			{
				std::cout << "TerrainType::BuildChunk internal error: buffer underflow" << std::endl;
				return;
			}
			for (int i=0; i<chunk_size; ++i)
			{
				stack[current_buffer].data[i] += stack[current_buffer+1].data[i];
			}
			stack[current_buffer].range_max += stack[current_buffer+1].range_max;
			stack[current_buffer].range_min += stack[current_buffer+1].range_min;
			break;
		//---------------------------------------------------------------------
		case Instruction::BUFFER_MERGE_SUB:
		//---------------------------------------------------------------------
			--current_buffer;
			if (current_buffer < 0)
			{
				std::cout << "TerrainType::BuildChunk internal error: buffer underflow" << std::endl;
				return;
			}
			for (int i=0; i<chunk_size; ++i)
			{
				stack[current_buffer].data[i] -= stack[current_buffer+1].data[i];
			}
			stack[current_buffer].range_max -= stack[current_buffer+1].range_min;
			stack[current_buffer].range_min -= stack[current_buffer+1].range_max;
			break;
		//---------------------------------------------------------------------
		case Instruction::BUFFER_MERGE_MUL:
		//---------------------------------------------------------------------
			--current_buffer;
			if (current_buffer < 0)
			{
				std::cout << "TerrainType::BuildChunk internal error: buffer underflow" << std::endl;
				return;
			}
			inv_range = 1.0f / (stack[current_buffer+1].range_max - stack[current_buffer+1].range_min);
			for (int i=0; i<chunk_size; ++i)
			{
				stack[current_buffer].data[i] *= (stack[current_buffer+1].data[i] - stack[current_buffer+1].range_min) * inv_range;
			}
			stack[current_buffer].range_max -= stack[current_buffer+1].range_min;
			stack[current_buffer].range_min -= stack[current_buffer+1].range_max;
			break;
		//---------------------------------------------------------------------
		case Instruction::BUFFER_DISCARD:
		//---------------------------------------------------------------------
			--current_buffer;
			if (current_buffer < 0)
			{
				std::cout << "TerrainType::BuildChunk internal error: buffer underflow" << std::endl;
				return;
			}
			break;
		//---------------------------------------------------------------------
		case Instruction::TERRAIN_NOISE:
		//---------------------------------------------------------------------
			// TODO: When we precalculate in the buffer, check if we should
			// just scaled-add the buffer, or if we have to calculate here.
			final_scale = (float)instruction->data.noise.size * one_over_scale;
			if (final_scale < MINIMUM_FINAL_SCALE)
			{
				break;
			}
			assert(Maths::IsPowerOfTwo(final_scale));
			one_over_final_scale = 1.0f / final_scale;

			NoiseSample::SampleNoise(
				instruction->data.noise.seed,
				(float)chunk->x * one_over_final_scale,
				(float)chunk->y * one_over_final_scale,
				instruction->data.noise.height,
				stack[current_buffer].data,
				chunk->width, chunk->height,
				(int)final_scale
			);
			// Note: the 1.2 is an approximate hack to compensate for any
			// enlargement that might happen when resampling with a lanczos
			// filter (a=3). It's an approximate sum of all the absolute peaks
			// on the curve that may occur.
			// Really we should investigate properly what the maximum
			// enlargement could be. Ask a mathematician.
			stack[current_buffer].range_min -= instruction->data.noise.height * 1.2;
			stack[current_buffer].range_max += instruction->data.noise.height * 1.2;
			break;
		//---------------------------------------------------------------------
		case Instruction::TERRAIN_SHAPE:
		//---------------------------------------------------------------------
			if (stack[current_buffer].range_max == stack[current_buffer].range_min)
			{
				break;
			}
			ShapeTerrain(stack[current_buffer].data, chunk->width*chunk->height,
				stack[current_buffer].range_min, stack[current_buffer].range_max,
				instruction->data.shape.values, instruction->data.shape.num_values);
			ShapeMinMax(&stack[current_buffer].range_min, &stack[current_buffer].range_max,
				instruction->data.shape.values, instruction->data.shape.num_values);
			break;
		//---------------------------------------------------------------------
		case Instruction::TEXTURE:
		//---------------------------------------------------------------------
			{
				bool probability = false;
				float target_probability = (instruction->data.texture_object.probability * 2.0f) - 1.0f;
				if (instruction->data.texture_object.probability < 1.0)
				{
					probability = true;
					final_scale = (float)(*it).data.texture_object.prob_size * one_over_scale;
					if (final_scale < MINIMUM_FINAL_SCALE)
					{
						break;
					}
					assert(Maths::IsPowerOfTwo(final_scale));
					one_over_final_scale = 1.0f / final_scale;

					for (int i=0; i<chunk_size; ++i)
					{
						probability_buffer[i] = 0.0f;
					}

					NoiseSample::SampleNoise(
						instruction->data.texture_object.prob_seed,
						(float)chunk->x * one_over_final_scale,
						(float)chunk->y * one_over_final_scale,
						1.0f,
						&probability_buffer[0],
						chunk->width, chunk->height,
						(int)final_scale
					);
				}

				bool gradient = false;
				if (instruction->data.texture_object.gradient_min !=
					instruction->data.texture_object.gradient_max)
				{
					gradient = true;
					CalculateGradients(stack[current_buffer].data, chunk->width, chunk->height, chunk->scale, &gradient_buffer[0]);
				}

				bool altitude = false;
				if ((*it).data.texture_object.altitude_min != instruction->data.texture_object.altitude_max)
				{
					altitude = true;
				}

				// TODO: It's possible that this is faster if we iterate multiple
				// passes with smaller amounts of data.
				for (int i=0; i<chunk_size; ++i)
				{
					if (probability)
					{
						if (probability_buffer[i] > target_probability)
						{
							continue;
						}
					}
					if (gradient)
					{
						if (
							gradient_buffer[i]
							< instruction->data.texture_object.gradient_min
							|| gradient_buffer[i]
							> instruction->data.texture_object.gradient_max)
						{
							continue;
						}
					}

					if (altitude)
					{
						if (stack[current_buffer].data[i] < instruction->data.texture_object.altitude_min || stack[current_buffer].data[i] > instruction->data.texture_object.altitude_max)
						{
							continue;
						}
					}

					chunk->textures[i] = instruction->data.texture_object.index;
				}
			}
			break;
		}
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

