/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "terrain/ProceduralTerrainSource.hh"
#include "terrain/TerrainBlender.hh"
#include "library/maths/GridSplit.hh"
#include "library/maths/Maths.hh"
#include <cassert>
#include <iostream>
#include <cmath>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Generates a chunk of a single terrain type.
 */
void ProceduralTerrainSource::GenerateSingleTerrainType(ProceduralTerrainType *type, int x, int y, int lod, TerrainData *data)
{
	int w = data->Width();
	int h = data->Height();

	ProceduralTerrainTypeBuildParameters parms;
	parms.x = x;
	parms.y = y;
	parms.width = w;
	parms.height = h;
	parms.scale = lod;
	parms.terrain = data->heights.RawElements();
	parms.textures = data->types.RawElements();

	type->BuildChunk(&parms);
}

/**
 * Creates TerrainData for the array of types.
 * Used as part of GenerateMultiTerrainType.
 * Unique types are de-duped, and we populate a list of unique ones so they can
 * be freed.
 */
void ProceduralTerrainSource::RenderTypes(const Array2D<ProceduralTerrainType*> &types, int x, int y, int w, int h, int lod, Array2D<TerrainData*> *data, std::vector<TerrainData*> *uniqueData)
{
	int typesSize = types.Width() * types.Height();
	for (int typeIndex = 0; typeIndex < typesSize; typeIndex += 1)
	{
		ProceduralTerrainType *type = types.at(typeIndex);

		// If we previously made this type, copy its pointer.
		bool copied = false;
		for (int checkTypeIndex = 0; checkTypeIndex < typeIndex; checkTypeIndex += 1)
		{
			if (types.at(checkTypeIndex) == type)
			{
				data->at(typeIndex) = data->at(checkTypeIndex);
				copied = true;
				break;
			}
		}
		// If we copied, skip making it.
		if (copied)
		{
			continue;
		}

		// Make it and add it to the unique list.
		TerrainData *newData = new TerrainData(w, h);
		if (type != nullptr)
		{
			GenerateSingleTerrainType(type, x, y, lod, newData);
		}
		data->at(typeIndex) = newData;
		uniqueData->push_back(newData);
	}

	if (uniqueData->size() < 1)
	{
		throw std::logic_error("No unique data rendered - probably some error in ProceduralTerrainSource::RenderTypes");
	}
}

/**
 * Generates a chunk of multiple types blended together.
 */
void ProceduralTerrainSource::GenerateMultiTerrainType(opti_MapCallback mapCallback, void* user_data, int mapScale, std::vector<ProceduralTerrainType>& terrainTypes, int x, int y, int lod, TerrainData *data)
{
	Vector2I dataWorldCoord(x, y);

	int w = data->Width();
	int h = data->Height();

	GridSplit gridSplit;
	TerrainBlender blender;
	
	assert(Maths::IsPowerOfTwo(lod));
	mapScale /= lod;
	assert(mapScale >= 2);
	// TODO: Handle smaller LOD. We could just sample the heights but we'd need to get textures somehow.
	
	// Get the map region that we need.
	int mapX = x / mapScale;
	int mapY = y / mapScale;
	int mapW = (w / mapScale) + 2;
	int mapH = (h / mapScale) + 2;
	
	Array2D<float> mapHeights(mapW, mapH);
	Array2D<int> mapTypes(mapW, mapH);
	mapCallback(user_data, mapX, mapY, mapW, mapH, mapTypes.RawElements(), mapHeights.RawElements());
	
	Array2D<ProceduralTerrainType*> map(mapW, mapH);
	for (int ele = 0; ele < mapW * mapH; ele += 1)
	{
		int type = mapTypes.at(ele);
		if (type < 0 || type > terrainTypes.size())
		{
			type = 0;
		}
		map.at(ele) = &terrainTypes[type];
	}

	std::vector<GridSplit::BoundInfo> bounds;
	gridSplit.GenerateBounds(x, y, x + w, y + h, mapScale, &bounds);

	for (int i = 0; i < bounds.size(); i += 1)
	{
		int boundsMapX = bounds[i].coordMin[0] / mapScale;
		int boundsMapY = bounds[i].coordMin[1] / mapScale;
		Array2D<ProceduralTerrainType*> types(map, boundsMapX, boundsMapY, 2, 2);
		Array2D<float> heights(mapHeights, boundsMapX, boundsMapY, 2, 2);

		std::vector<TerrainData*> uniqueData;
		Array2D<TerrainData*> mapGridData(2, 2);

		Vector2I subChunkSize = bounds[i].coordMax - bounds[i].coordMin;
		Vector2I offsetInDest = bounds[i].coordMin - dataWorldCoord;
		RenderTypes(types, bounds[i].coordMin[0], bounds[i].coordMin[1], subChunkSize[0], subChunkSize[1], lod, &mapGridData, &uniqueData);

		if (uniqueData.size() == 1)
		{
			data->SetRegion(*uniqueData[0], offsetInDest[0], offsetInDest[1]);
		}
		else
		{
			TerrainBlender::TerrainBlendInfo info;
			info.data[0] = mapGridData.at(0, 0);
			info.data[1] = mapGridData.at(1, 0);
			info.data[2] = mapGridData.at(0, 1);
			info.data[3] = mapGridData.at(1, 1);

			info.fracMin = bounds[i].fracMin;
			info.fracMax = bounds[i].fracMax;

			info.gridSize = mapScale;

			blender.BlendTerrain(info, bounds[i].coordMin, data, offsetInDest);
		}

		// TODO: This needs to handle the case when we're not blending the whole tile.
		blender.AddHeight(heights, bounds[i].fracMin, bounds[i].fracMax, data, offsetInDest, subChunkSize);

		// Tidy up the unique data we created.
		for (int j = 0; j < uniqueData.size(); j += 1)
		{
			delete uniqueData.at(j);
		}
	}
}

/**
 * Flattens the boundary with water so that it occurs on triangle boundaries.
 * Note that it requires an extra border of 1 be generated around the chunk,
 * because we need to know if a triangle on the other side is below or above
 * water to know if an edge is a water boundary.
 */
void ProceduralTerrainSource::FlattenWaterBoundary(TerrainData *data)
{
	int w = data->Width();
	int h = data->Height();
	Array2D<float> *heights = &data->heights;

	for (int y = 1; y < h - 1; y += 1)
	{
		for (int x = 1; x < w - 1; x += 1)
		{
			if (heights->at(x, y) < 0
				&& (
					   heights->at(x+1, y) > 0
					|| heights->at(x-1, y) > 0
					|| heights->at(x, y+1) > 0
					|| heights->at(x, y-1) > 0
					)
				)
			{
				heights->at(x, y) = 0;
			}
		}
	}
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

