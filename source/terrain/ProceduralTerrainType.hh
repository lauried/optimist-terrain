/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <list>
#include <vector>
#include <string>
#include <stdexcept>
#include "library/maths/IntegerMaths.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/*
-------------------------------------------------------------------------------
Lines beginning with # are comments, all others are taken as instructions.
Blank lines are ignored. The loader will not accept invalid instructions.

Commands:

buffer new
	Creates a new buffer stacked on top of the current. The code should warn if
	the instructions end with more than the initial buffer.

buffer duplicate
	Creates a new buffer stacked on top of the current. The buffer created is a
	duplicate of the current buffer.

buffer merge add
	Merges the current buffer down by adding it to the one below. If there is
	no buffer below then the code should halt and show an error.

buffer merge sub
	Merges the current buffer down by subtracting it fron the one below. If
	there is no buffer below then the code should halt and show an error.

buffer merge mul
	Merges the current buffer down by multiplying it by the one below. The
	result is scaled down by the top buffer's maximum possible range of values,
	so that the range of the buffer below is not changed.

buffer discard
	Discards the top buffer and returns to using the one below. This is useful
	if we want to create a side buffer for texture or object placement but have
	no use for it in the terrain height.


terrain noise <size> <height> <seed>
	Adds noise to the current buffer. Size is the feature size, height is the
	vertical scale applied to the noise and seed chooses the set of noise to
	use.

terrain shape <value> <value> <value> ...
	Shapes the current buffer. The values represent equally placed points on a
	graph of input to output heights. The values are equally spaced for input,
	and specify the output height at that position.


texture <texture>
[ probability <prob> <size> <seed> ]
[ gradient ( <minimum> to <maximum> | (above|below) <limit> ) ]
[ altitude ( <minimum> to <maximum> | (above|below) <limit> ) ]
	Places texture based on any combination of a random noise field, the
	current gradient and the current altitude. The probability section
	generates noise at the size and seed and then places where the normalized
	height of the new field is less than the probability value. The height must
	be normalized to the 0.0 to 1.0 range that the probability parameter takes
	(or in code, the probability value is adjusted to the range -1.0 to 1.0).
	Sections in square brackets can be in any order.

Notes:

- Noise size is a positive integer power of 2.
- Noise height is a floating point in final altitude coordinates, to scale the
  noise from a range -1.0 - 1.0 up to the desired size.
- Noise seed is an integer.

- Shape values are floating point scale factors, representing evenly spaced
  points on a graph mapping input values to output values.

- Probability is a floating point 0.0 to 1.0.
- Probability size is a positive integer power of 2.
- Probability seed is an integer.
- Gradient is floating point 0.0 (flat) to 1.0 (vertical).
  Terrain gradient is 1.0 - normal_z.
- Altitude is floating point, in the final altitude coordinates.

Terrain generation is order specific. Therefore it is acceptable for
texture placement to be performed on the intermediate state of the
buffer at their position in the file.

We could implement little side-buffers to generate for example random clusters
of forest by generating some terrain in a new buffer and then specifying an
altitude minimum or maximum. Alternately the frequency and probability do this
automatically for one size of noise.

The texturing is just some means of outputting some arbitrary index data with
the heights. Future expansion could support an arbitrary number of textures
weighted simultaneously.
-------------------------------------------------------------------------------
*/

/*
-------------------------------------------------------------------------------
Possible Optimizations:
- If noise of a specific frequency is used more than once it might be worth
  precaching it. This does increase the memory usage, so there could be a limit
  to the size of the cache per tile generation thread.
  Because objects of this class represent a type of terrain rather than a tile
  of it, we can have a parameter specifying an externally provided buffer and
  its size and use that however we see fit. alternately we can encapsulate the
  destination data and buffers in another class.
- When noise is being added with a scaling factor greater than the largest
  cached lanczos kernel, it may be acceptable to generate it with a lower
  scaling factor and then enlarge the rest of the way linearly (or cosine).
- The first texture should never be checked, and always fill the entire tile.
-------------------------------------------------------------------------------
*/

class ProceduralTerrainTypeBuildParameters {
public:
	// coordinates and size in units of our tile
	int       x;
	int       y;
	int       width;
	int       height;
	// power of 2 scale. multiply x,y,width,height by this to get world coords
	int       scale;
	// each contains width*height elements
	float    *terrain;
	int      *textures;

	// Throws an exception if the parameters are not valid.
	bool Validate()
	{
		if (width < 1)
		{
			throw std::out_of_range("ProceduralTerrainType - width less than 1");
		}
		if (height < 1)
		{
			throw std::out_of_range("ProceduralTerrainType - height less than 1");
		}
		if (!IntegerMaths::IsPowerOfTwo(scale))
		{
			throw std::logic_error("ProceduralTerrainType - scale is not power of 2");
		}

		terrain  = new float[width*height];
		textures = new int[width*height];
	}
};

class ProceduralTerrainType {
public:

	ProceduralTerrainType();
	~ProceduralTerrainType();

	void LoadData(const char *data, size_t dataSize);
	void UnLoad();
	bool IsLoaded() { return is_loaded; }
	int  GetMaxBuffers() { return max_buffers; }

	void PrintInstructions();
	void PrintNoiseList();

	void BuildChunk(ProceduralTerrainTypeBuildParameters *chunk, float *buffer=NULL, int buffer_size=0);

private:
	bool is_loaded;
	static const int kMaxShapeValues = 16;

	class Instruction {
		public:

		typedef enum {
			BUFFER_NEW,
			BUFFER_DUPLICATE,
			BUFFER_MERGE_ADD,
			BUFFER_MERGE_SUB,
			BUFFER_MERGE_MUL,
			BUFFER_DISCARD,
			TERRAIN_NOISE,
			TERRAIN_SHAPE,
			TEXTURE,
			OBJECT
		} InstructionType;

		InstructionType type;
		// store line number for debug output
		int line_number;
		union {
			struct {
				int   size;
				float height;
				int   seed;
			} noise;
			struct {
				int   num_values;
				float values[kMaxShapeValues];
			} shape;
			struct {
				int   index;
				// Probability of 1.0 means we don't do this calculation.
				float probability;
				int   prob_size;
				int   prob_seed;
				// If these max/min values are equal we treat it as having no
				// gradient or altitude limits.
				float gradient_min, gradient_max;
				float altitude_min, altitude_max;
			} texture_object;
		} data;
	};

	std::vector<Instruction> instructions;

	int num_buffers; // temporary use when parsing.
	int max_buffers; // maximum buffers used when building.

	// true if any texture or object instruction uses a gradient, else false.
	bool uses_gradient;

	void hClearData();

    // TODO: Come up with a better name for this.
	struct UniqueNoise {
		int size;
		int seed;
		int count;
	};

	std::list<UniqueNoise> unique_noise_counts;

	void hAddUniqueNoiseCount(int size, int seed);
	void hSortUniqueNoise();
	static bool hCompareUniqueNoise(UniqueNoise &a, UniqueNoise &b);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
