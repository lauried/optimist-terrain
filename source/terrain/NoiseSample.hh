/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class NoiseSample {
public:

	/**
	 * Upsamples random noise from a 2D pseudo-random noise field into a
	 * buffer of floats. Values are added to the buffer rather than set, so
	 * that we can blend in multiple scales of noise.
	 * @param sourcePlane
	 * The noise data field is really 3D which we take a single plane from.
	 * This parameter determines the third axis. It is used so that we can use
	 * several separate sets rather than always sample from the one set.
	 * @param sourceX
	 * The x coordinate within the source noise to sample from. The noise
	 * sampled will start here and end at a higher value coordinate.
	 * @param sourceY
	 * As sourceX but the other dimension.
	 * @param heightScale
	 * Amount by which to scale the float values before adding them to the
	 * buffer.
	 * @param dest
	 * Pointer to a buffer of floats. There must be room for
	 * destWidth * destHeight floats in the buffer.
	 * @param destWidth
	 * Width of the destination buffer. This doesn't have to be a power of 2,
	 * thus allowing us to have power-of-2 sided tiles with a border around
	 * for calculating normals within the single tile, for example.
	 * @param destHeight
	 * Height of the destination buffer. Can be any value, not necessarily
	 * power of 2.
	 * @param scaleToDest
	 * Scaling factor when upsampling from the noise to the destination
	 * buffer. Each pixel in the source corresponds to this many pixels in the
	 * destination. This value MUST be a power of 2.
	 * The area of noise that we sample is therefore
	 * destWidth/scaleToDest x destHeight/scaleToDest.
	 */
	static void SampleNoise(
		int    sourcePlane,
		float  sourceX,
		float  sourceY,
		float  heightScale,
		float *dest,
		int    destWidth,
		int    destHeight,
		int    scaleToDest
	);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
