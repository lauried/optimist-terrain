/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include "terrain/ProceduralTerrainType.hh"
#include "terrain/TerrainData.hh"
#include "terraingen.h"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class ProceduralTerrainSource {
public:
	void GenerateMultiTerrainType(opti_MapCallback mapCallback, void* user_data, int mapScale, std::vector<ProceduralTerrainType>& terrainTypes, int x, int y, int lod, TerrainData *data);
	void GenerateSingleTerrainType(ProceduralTerrainType *type, int x, int y, int lod, TerrainData *data);

private:
	struct BoundInfo {
		Vector2I coordMin, coordMax;
		Vector2F fracMin, fracMax;
	};

	void FlattenWaterBoundary(TerrainData *data);
	void RenderTypes(const Array2D<ProceduralTerrainType*> &types, int x, int y, int w, int h, int lod, Array2D<TerrainData*> *data, std::vector<TerrainData*> *uniqueData);
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
