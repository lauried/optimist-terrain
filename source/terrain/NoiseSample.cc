/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "terrain/NoiseSample.hh"
#include <cmath>
#include <map>
#include <stdexcept>
#include "library/maths/Maths.hh"
#include "library/maths/IntegerMaths.hh"
#include "library/maths/RandomField.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Vector Length (2D)
//-----------------------------------------------------------------------------
inline float Length(float x, float y)
{
	return std::sqrt(x*x+y*y);
}

//-----------------------------------------------------------------------------
// Sinc Table
// While a sine table didn't offer any speedup, precalculating sinc values
// did.
//-----------------------------------------------------------------------------
class SincTable {
private:
	static const int   cTableSize = 0x400;
	static const int   cTableMask = cTableSize - 1;
	static constexpr float A_TO_TABLE = cTableSize / 3.0f;

	float mTable[cTableSize];
	// sinc function
	inline float sinc_real(float f)
	{
		float x = f*M_PI;
		return sin(x)/x;
	}
	// table lookup
	inline float sinc_table(float f)
	{
		if (f < 0) f *= -1;
		return mTable[(int)(f*A_TO_TABLE)&cTableMask];
	}

public:
	// constructor builds table
	SincTable()
	{
		for (int i=0; i<cTableSize; ++i)
		{
			mTable[i] = sinc_real((float)i/A_TO_TABLE);
			//if (isnan(mTable[i]))
			//	mTable[i] = 1.0f;
		}
	}
	// public lookup (this lets us compare sinc_table with sinc_real)
	inline float sinc(float f)
	{
		return sinc_table(f);
	}
};

SincTable gSincTable;

//-----------------------------------------------------------------------------
// Lanczos Filter Kernel Precalculation
// When the scale when upsampling is a power of 2, the fractions of source
// corresponding to the destination pixel repeat over every whole source
// pixel. The kernel depends only on the fraction and therefore we can
// precalculate the set of kernels when there is a small enough set.
//-----------------------------------------------------------------------------
class LanczosKernelCache {
public:
	static const int cMatrixWidth  = 6;
	static const int cMatrixOffset = 2;
	static const int cMatrixSize   = cMatrixWidth * cMatrixWidth;
	static constexpr float cOneOverA   = 1.0f / 3.0f;

	static void CalculateKernel(float *matrix, float fracX, float fracY)
	{
		// first calculate the lengths to use per axis
		// these are lengths from the current fractional point to the
		// nearest source sample.
		float lengthsX[cMatrixWidth] = { fracX+2, fracX+1, fracX, 1-fracX, (1-fracX)+1, (1-fracX)+2 };
		float lengthsY[cMatrixWidth] = { fracY+2, fracY+1, fracY, 1-fracY, (1-fracY)+1, (1-fracY)+2 };

		// then calculate the matrix
		for (int y=0; y<cMatrixWidth; ++y)
		{
			for (int x=0; x<cMatrixWidth; ++x)
			{
				float len = Length(lengthsX[x], lengthsY[y]);
				if (len >= 3)
					matrix[y*cMatrixWidth+x] = 0;
				else if (len == 0)
					matrix[y*cMatrixWidth+x] = 1;
				else
					matrix[y*cMatrixWidth+x] = gSincTable.sinc(len)*gSincTable.sinc(len*cOneOverA);
			}
		}
	}

	LanczosKernelCache(int size)
	{
		if (!IntegerMaths::IsPowerOfTwo(size))
		{
			throw std::logic_error("LanczosKernelCache not power-of-2 sized");
		}
		mData     = new float[size * size * cMatrixSize];
		mSize     = size;
		mSizeMask = mSize - 1;
		float invScale = 1.0f / (float)mSize;

		for (int y=0; y<mSize; ++y)
		{
			for (int x=0; x<mSize; ++x)
			{
				float *kernel = &mData[(y*mSize + x) * cMatrixSize];
				CalculateKernel(kernel, (float)x * invScale, (float)y * invScale);
			}
		}

	}

	~LanczosKernelCache()
	{
		delete[] mData;
	}

	inline const float *GetKernel(int x, int y)
	{
		x &= mSizeMask;
		y &= mSizeMask;
		return &mData[(y*mSize + x) * cMatrixSize];
	}

private:
	int    mSize;
	int    mSizeMask;
	float *mData;
};

//-----------------------------------------------------------------------------
// Manage precalculation of the Lanczos Kernel Caches for different sizes
//-----------------------------------------------------------------------------
class LanczosKernelCacheManager {
public:
	LanczosKernelCacheManager()
	{
	}
	~LanczosKernelCacheManager()
	{
		std::map<int, LanczosKernelCache*>::iterator it;
		for (it=mCaches.begin(); it!=mCaches.end(); ++it)
		{
			delete it->second;
		}
		mCaches.clear();
	}

	LanczosKernelCache *GetCache(int size)
	{
		if (!IntegerMaths::IsPowerOfTwo(size))
		{
			throw std::logic_error("GetCache - size is not power of 2");
		}
		if(size < 2)
		{
			throw std::logic_error("GetCache - size is less than 2");
		}

		if (size > cMaxLevel)
			return NULL;

		std::map<int, LanczosKernelCache*>::iterator it = mCaches.find(size);
		if (it == mCaches.end())
		{
			LanczosKernelCache *cache = new LanczosKernelCache(size);
			mCaches.insert(std::pair<int, LanczosKernelCache*>(size, cache));
			return cache;
		}
		else
		{
			return it->second;
		}
	}

private:
	static const int cMaxLevel = 128;

	std::map<int, LanczosKernelCache*> mCaches;
};

//-----------------------------------------------------------------------------
// Cached Interpolate
//-----------------------------------------------------------------------------
inline float Interpolate_Lanczos_Cached
(
	const float* source, int sourceWidth,
	int sourceSampleX, int sourceSampleY,
	int fracX, int fracY, int scale, float invScale,
	LanczosKernelCache* cache
)
{
	const float* matrix = cache->GetKernel(fracX, fracY);

	float result = 0;
	for (int y=0, yy=sourceSampleY-LanczosKernelCache::cMatrixOffset; y<LanczosKernelCache::cMatrixWidth; ++y, ++yy)
	{
		for (int x=0, xx=sourceSampleX-LanczosKernelCache::cMatrixOffset; x<LanczosKernelCache::cMatrixWidth; ++x, ++xx)
		{
			result += matrix[y*LanczosKernelCache::cMatrixWidth+x] * source[yy*sourceWidth+xx];
		}
	}

	return result;
}

//-----------------------------------------------------------------------------
// Non-cached Interpolate
//-----------------------------------------------------------------------------

inline float Interpolate_Lanczos
(
	const float* source, int sourceWidth,
	int sourceSampleX, int sourceSampleY,
	int fracX, int fracY, int scale, float invScale,
	LanczosKernelCache* cache
)
{
	float fFracX = (float)fracX * invScale;
	float fFracY = (float)fracY * invScale;

	float matrix[LanczosKernelCache::cMatrixSize];
	LanczosKernelCache::CalculateKernel(matrix, fFracX, fFracY);

	float result = 0;
	for (int y=0, yy=sourceSampleY-LanczosKernelCache::cMatrixOffset; y<LanczosKernelCache::cMatrixWidth; ++y, ++yy)
	{
		for (int x=0, xx=sourceSampleX-LanczosKernelCache::cMatrixOffset; x<LanczosKernelCache::cMatrixWidth; ++x, ++xx)
		{
			result += matrix[y*LanczosKernelCache::cMatrixWidth+x] * source[yy*sourceWidth+xx];
		}
	}

	return result;
}

//-----------------------------------------------------------------------------
// Resample
// Resamples one set of data to another, using a given interpolate func.
//-----------------------------------------------------------------------------

typedef float(*tInterpolateFunc)
(
	const float* source, int sourceWidth,
	int sourceSampleX, int sourceSampleY,
	int fracX, int fracY, int scale, float invScale,
	LanczosKernelCache* cache
);

inline void Resample
(
	const float* source, int sourceWidth, int sourceHeight,
	float* dest, int destWidth, int destHeight,
	float sourceX, float sourceY, int scaleToDest,
	tInterpolateFunc interpolateFunc, LanczosKernelCache* cache
)
{
	if (!IntegerMaths::IsPowerOfTwo(scaleToDest))
	{
		throw std::logic_error("Resample - scaleToDest is not a power of 2");
	}

	float invScale = 1.0 / (float)scaleToDest;
	int sourceSampleXStart = (int)floor(sourceX);
	int sourceSampleYStart = (int)floor(sourceY);
	int fracXstart = (int)((sourceX - floor(sourceX))*scaleToDest);
	int fracYstart = (int)((sourceY - floor(sourceY))*scaleToDest);
	int fracX, fracY, sourceSampleX, sourceSampleY;

	fracY = fracYstart;
	sourceSampleY = sourceSampleYStart;
	for (int y=0, i=0; y<destHeight; ++y)
	{
		fracX = fracXstart;
		sourceSampleX = sourceSampleXStart;
		for (int x=0;x<destWidth; ++x, ++i)
		{
			dest[i] += interpolateFunc(
							source, sourceWidth,
							sourceSampleX, sourceSampleY,
							fracX, fracY, scaleToDest, invScale,
							cache);

			if (++fracX >= scaleToDest)
			{
				fracX = 0;
				++sourceSampleX;
			}
		}

		if (++fracY >= scaleToDest)
		{
			fracY = 0;
			++sourceSampleY;
		}
	}
}

//-----------------------------------------------------------------------------
// Generate noise and invoke resampling
//-----------------------------------------------------------------------------

LanczosKernelCacheManager gKernelCache;

void NoiseSample::SampleNoise
(
	int sourcePlane, float sourceX, float sourceY, float heightScale,
	float* dest, int destWidth, int destHeight, int scaleToDest
)
{
	const int cBorderSize = 2;

	if (scaleToDest == 1)
	{
		for (int y=0, i=0; y<destHeight; ++y)
		{
			for (int x=0; x<destWidth; ++x, ++i)
			{
				dest[i] +=
					((RandomField::Calculate(x+sourceX, y+sourceY, sourcePlane)
						& 0xffff)/65535.0f) * heightScale;
			}
		}
		return;
	}

	if (!IntegerMaths::IsPowerOfTwo(scaleToDest))
	{
		throw std::logic_error("NoiseSample::SampleNoise - scaleToDest is not a power of 2");
	}

	// TODO:
	// If scale is 1 and sourceX and sourceY are whole numbers, then just copy.

	float sourceXmax = sourceX + (float)destWidth/(float)scaleToDest;
	float sourceYmax = sourceY + (float)destHeight/(float)scaleToDest;

	int sourceTileXmin = (int)floor(sourceX) - cBorderSize;
	int sourceTileYmin = (int)floor(sourceY) - cBorderSize;
	int sourceTileXmax = (int)ceil(sourceXmax) + cBorderSize;
	int sourceTileYmax = (int)ceil(sourceYmax) + cBorderSize;
	int sourceTileWidth = (sourceTileXmax - sourceTileXmin) + cBorderSize*2;
	int sourceTileHeight = (sourceTileYmax - sourceTileYmin) + cBorderSize*2;

	// Generate the source noise tile which would cover the destination,
	// plus its border.
	float sourceTile[sourceTileWidth*sourceTileHeight];
	for (int y=0, i=0; y<sourceTileHeight; ++y)
	{
		for (int x=0; x<sourceTileWidth; ++x, ++i)
		{
			float sample = (RandomField::Calculate(x+sourceTileXmin, y+sourceTileYmin, sourcePlane) & 0xffff);
			sample -= 32768.0f;
			sample /= 32768.0f;
			sourceTile[sourceTileWidth*y + x] = sample * heightScale;
		}
	}

	// Use a cache if we have one generated for this scale
	tInterpolateFunc interpolateFunc = Interpolate_Lanczos;
	LanczosKernelCache* cache = gKernelCache.GetCache(scaleToDest);
	if (cache != NULL)
		interpolateFunc = Interpolate_Lanczos_Cached;

	// Resample the source to the destination (sourceX and sourceY have to be
	// in the source tile's coordinates)
	float xofs = sourceX-(float)sourceTileXmin;
	float yofs = sourceY-(float)sourceTileYmin;
	Resample(
		sourceTile, sourceTileWidth, sourceTileHeight,
		dest, destWidth, destHeight,
		xofs, yofs, scaleToDest,
		interpolateFunc, cache);
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

