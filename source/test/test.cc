/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include <cassert>
#include <cmath>
#include <fstream>
#include <iostream>
#include "terraingen.h"

using std::max;

// Copy a region from one tile to another.
template<typename T>
void copyRegion(const T* source, int sw, T* dest, int dw, int fromX, int fromY, int fromW, int fromH, int toX, int toY)
{
	for (int y = 0; y < fromH; y += 1)
	{
		int sourceY = (fromY + y) * sw;
		int destY = (toY + y) * dw;
		for (int x = 0; x < fromW; x += 1)
		{
			dest[destY + toX + x] = source[sourceY + fromX + x];
		}
	}
}

template<typename T>
bool compareBuffers(const T* a, const T* b, int width, int height, const char *type)
{
	bool difference = false;
	for (int y = 0; y < height; y += 1)
	{
		int yofs = y * width;
		for (int x = 0; x < width; x += 1)
		{
			int ofs = yofs + x;
			if (a[ofs] != b[ofs])
			{
				std::cout << type << " mismatch at " << x << "," << y << " (" << a[ofs] << " != " << b[ofs] << ")" << std::endl;
				difference = true;
			}
		}
	}
	return difference;
}

template<typename T>
void diffBuffers(const T* a, const T* b, T* dest, int width, int height)
{
	for (int i = 0; i < width * height; i += 1)
	{
		dest[i] = a[i] - b[i];
	}
}

void writeHeightmapTarga(float* heights, int w, int h, const char* filename)
{
	std::ofstream file;
	file.open(filename, std::ios::binary);
	
	unsigned char header[] = {
		0, // no image id field
		0, // no colour map
		2, // uncompressed true-colour image
		0, 0, 0, 0, 0, // empty colour map specification
		0, 0, // x origin zero
		0, 0, // y origin zero
		(unsigned char)((w & 0xff)),
		(unsigned char)((w & 0xff00) >> 8),
		(unsigned char)((h & 0xff)),
		(unsigned char)((h & 0xff00) >> 8),
		24, // 24 bpp
		32, // flag for top left origin
	};
	
	for (int index = 0; index < sizeof(header); index += 1)
	{
		file << header[index];
	}
	
	for (int index = 0; index < w * h; index += 1)
	{
		unsigned char val = (unsigned char)((heights[index] + 1.0f) * 128.0f);
		file << val << val << val;
	}
	
	file.close();
}

void clearHeights(float *heights, int size)
{
	for (int index = 0; index < size; index += 1)
	{
		heights[index] = 0;
	}
}

struct UserData {
	int map_type;
	int other_types[4];
};

void map_callback(void* user_data, int x, int y, int w, int h, int* terrain_types, float *coarse_heights)
{
	UserData data = *(UserData*)user_data;
	opti_generateSingleTerrain(data.map_type, x, y, w, h, 1, coarse_heights, terrain_types);
	// Map them with the terrain types that we got from registering.
	for (int index = 0; index < w * h; index += 1)
	{
		terrain_types[index] = data.other_types[terrain_types[index]];
	}
}

int main(int argc, char *argv[])
{
	int error_code = 0;
	
	std::cout << "Starting tests" << std::endl;
	
	// Check that last error is sensible on init.
	auto error = opti_getLastError();
	assert(error->line == -1);
	assert(error->message[0] == '\0');
	
	UserData data;
	
	data.map_type = opti_registerTerrainType(
		"terrain noise 16 1 1\n"
		"texture 0\n"
		"texture 1 probability 0.5 4 234\n"
		"texture 2 probability 0.33 4 345\n"
		"texture 3 probability 0.25 4 456\n"
	);
	
	// TODO: Some different types.
	data.other_types[0] = opti_registerTerrainType("terrain noise 2 1 8\ntexture 0\n");
	data.other_types[1] = opti_registerTerrainType("terrain noise 2 1 8\ntexture 0\n");
	data.other_types[2] = opti_registerTerrainType("terrain noise 2 1 8\ntexture 0\n");
	data.other_types[3] = opti_registerTerrainType("terrain noise 2 1 8\ntexture 0\n");
	
	// Test Reloading.
	assert(opti_updateTerrainType("terrain noise  4 1 8\n", data.other_types[1]) == data.other_types[1]);
	assert(opti_updateTerrainType("terrain noise  8 1 8\n", data.other_types[2]) == data.other_types[2]);
	assert(opti_updateTerrainType("terrain noise 16 1 8\n", data.other_types[3]) == data.other_types[3]);

	// Check for stuff that should cause an error.
	int nonExistantType = max(data.other_types[0], max(data.other_types[1], max(data.other_types[2], data.other_types[3]))) + 1;
	float heightsTest[16 * 16];
	int texturesTest[16 * 16];
	// Free non existant type.
	assert(opti_freeTerrainType(nonExistantType) == OPTI_TERRAIN_GRAMMAR_NON_EXISTENT);
	// Generate non existant terrain type.
	assert(opti_generateSingleTerrain(nonExistantType, 0, 0, 16, 16, 1, heightsTest, texturesTest) == OPTI_TERRAIN_INVALID_TERRAIN_TYPE);
	// Generate with zero dimension.
	assert(opti_generateSingleTerrain(data.map_type, 0, 0, 16, 0, 1, heightsTest, texturesTest) == OPTI_TERRAIN_INVALID_HEIGHT);
	assert(opti_generateSingleTerrain(data.map_type, 0, 0, 0, 16, 1, heightsTest, texturesTest) == OPTI_TERRAIN_INVALID_WIDTH);
	assert(opti_generateMultiTerrain(map_callback, &data, 32, 0, 0, 16, 0, 1, heightsTest, texturesTest) == OPTI_TERRAIN_INVALID_HEIGHT);
	assert(opti_generateMultiTerrain(map_callback, &data, 32, 0, 0, 0, 16, 1, heightsTest, texturesTest) == OPTI_TERRAIN_INVALID_WIDTH);
	// Scale / LOD < 2
	assert(opti_generateMultiTerrain(map_callback, &data, 16, 0, 0, 16, 16, 16, heightsTest, texturesTest) == OPTI_TERRAIN_INVALID_LOD_AND_SCALE);
	assert(opti_generateMultiTerrain(map_callback, &data, 1, 0, 0, 16, 16, 1, heightsTest, texturesTest) == OPTI_TERRAIN_INVALID_LOD_AND_SCALE);
	// LOD non power of 2.
	assert(opti_generateSingleTerrain(data.map_type, 0, 0, 16, 16, 7, heightsTest, texturesTest) == OPTI_TERRAIN_INVALID_LOD);
	assert(opti_generateSingleTerrain(data.map_type, 0, 0, 16, 16, -6, heightsTest, texturesTest) == OPTI_TERRAIN_INVALID_LOD);
	assert(opti_generateMultiTerrain(map_callback, &data, 16, 0, 0, 16, 16, 7, heightsTest, texturesTest) == OPTI_TERRAIN_INVALID_LOD);
	assert(opti_generateMultiTerrain(map_callback, &data, 16, 0, 0, 16, 16, -6, heightsTest, texturesTest) == OPTI_TERRAIN_INVALID_LOD);

	// Generate single map tile.
	float px = 48;
	float py = 48;
	float heights1[32 * 32];
	int textures1[32 * 32];
	opti_generateSingleTerrain(data.map_type, px, py, 32, 32, 1, heights1, textures1);
	
	// Test that 4 small tiles match the single generated tile.
	float heights2[32 * 32];
	int textures2[32 * 32];
	float heightsSmall[16 * 16];
	int texturesSmall[16 * 16];
	
	opti_generateSingleTerrain(data.map_type, px, py, 16, 16, 1, heightsSmall, texturesSmall);
	copyRegion(heightsSmall, 16, heights2, 32, 0, 0, 16, 16, 0, 0);
	copyRegion(texturesSmall, 16, textures2, 32, 0, 0, 16, 16, 0, 0);
	
	opti_generateSingleTerrain(data.map_type, px + 16, py, 16, 16, 1, heightsSmall, texturesSmall);
	copyRegion(heightsSmall, 16, heights2, 32, 0, 0, 16, 16, 16, 0);
	copyRegion(texturesSmall, 16, textures2, 32, 0, 0, 16, 16, 16, 0);
	
	opti_generateSingleTerrain(data.map_type, px, py + 16, 16, 16, 1, heightsSmall, texturesSmall);
	copyRegion(heightsSmall, 16, heights2, 32, 0, 0, 16, 16, 0, 16);
	copyRegion(texturesSmall, 16, textures2, 32, 0, 0, 16, 16, 0, 16);
	
	opti_generateSingleTerrain(data.map_type, px + 16, py + 16, 16, 16, 1, heightsSmall, texturesSmall);
	copyRegion(heightsSmall, 16, heights2, 32, 0, 0, 16, 16, 16, 16);
	copyRegion(texturesSmall, 16, textures2, 32, 0, 0, 16, 16, 16, 16);
	
	writeHeightmapTarga(heights1, 32, 32, "build/heights1.tga");
	writeHeightmapTarga(heights2, 32, 32, "build/heights2.tga");
	
	if (
		compareBuffers(heights1, heights2, 32, 32, "height")
		|| compareBuffers(textures1, textures2, 32, 32, "texture")
	) {
		error_code = 1;
	}
	
	// Generate single tile map with LOD.
	float heightsLod[16 * 16];
	int texturesLod[16 * 16];
	opti_generateSingleTerrain(data.map_type, px, py, 16, 16, 2, heightsLod, texturesLod);
	writeHeightmapTarga(heightsLod, 16, 16, "build/heightsLod.tga");
	// Compare it to alternate samples from the full size version.
	float heightsReduced[16 * 16];
	for (int y = 0; y < 16; y += 1)
	{
		int ay = y * 2;
		for (int x = 0; x < 16; x += 1)
		{
			int ax = x * 2;
			heightsReduced[y * 16 + x] = heights1[ay * 32 + ax];
		}
	}
	if (compareBuffers(heightsLod, heightsReduced, 16, 16, "height with lod"))
	{
		error_code = 1;
	}
	
	// Generate some multi terrain.
	float heightsMulti1[96 * 96];
	int texturesMulti1[96 * 96];
	opti_generateMultiTerrain(map_callback, &data, 16, 0, 0, 96, 96, 1, heightsMulti1, texturesMulti1);
	writeHeightmapTarga(heightsMulti1, 96, 96, "build/heightsMulti1.tga");
	
	float heightsMulti2[96 * 96];
	int texturesMulti2[96 * 96];
	opti_generateMultiTerrain(map_callback, &data, 16, 8, 8, 96, 96, 1, heightsMulti2, texturesMulti2);
	writeHeightmapTarga(heightsMulti2, 96, 96, "build/heightsMulti2.tga");
	
	float compare1[80 * 80];
	float compare2[80 * 80];
	copyRegion(heightsMulti1, 96, compare1, 80, 8, 8, 80, 80, 0, 0);
	copyRegion(heightsMulti2, 96, compare2, 80, 0, 0, 80, 80, 0, 0);
	if (compareBuffers(compare1, compare2, 80, 80, "multiheight"))
	{
		error_code = 1;
	}
	
	float diff[80 * 80];
	diffBuffers(compare1, compare2, diff, 80, 80);
	writeHeightmapTarga(diff, 80, 80, "build/multiDiff.tga");
	
	// Generate multi LOD.
	float heightsMultiLod[48 * 48];
	int texturesMultiLod[48 * 48];
	opti_generateMultiTerrain(map_callback, &data, 16, 8, 8, 48, 48, 2, heightsMultiLod, texturesMultiLod);
	writeHeightmapTarga(heightsMultiLod, 48, 48, "build/heightsMultiLod.tga");
	
	std::cout << "Finished testing" << std::endl;
	
	return 0;
}
