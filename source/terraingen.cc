/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include <cstring>
#include <iostream>
#include <vector>
#include "terraingen.h"
#include "library/maths/IntegerMaths.hh"
#include "library/text/ParseException.hh"
#include "terrain/TerrainData.hh"
#include "terrain/ProceduralTerrainType.hh"
#include "terrain/ProceduralTerrainSource.hh"

class ErrorWrapper {
public:
	opti_TerrainGrammarError error;

	ErrorWrapper()
	{
		error.line = -1;
		error.message[0] = '\0';
	}
};

static std::vector<opti::ProceduralTerrainType> terrain_types;
static ErrorWrapper last_grammar_error;

int opti_registerTerrainType(const char* grammar)
{
	int index;
	
	for (index = 0; index < terrain_types.size(); index += 1)
	{
		if (!terrain_types[index].IsLoaded())
		{
			break;
		}
	}
	
	if (index >= terrain_types.size())
	{
		terrain_types.push_back(opti::ProceduralTerrainType());
	}
	
	try
	{
		terrain_types[index].LoadData(grammar, std::strlen(grammar));
		return index;
	}
	catch (opti::ParseException &e)
	{
		last_grammar_error.error.line = e.line;
		std::strncpy(last_grammar_error.error.message, e.message.c_str(), sizeof(last_grammar_error.error.message));
		last_grammar_error.error.message[sizeof(last_grammar_error.error.message) - 1] = '\0';
		
		return OPTI_TERRAIN_GRAMMAR_ERROR;
	}
}

int opti_updateTerrainType(const char* grammar, int terrain_type)
{
	if (terrain_type < 0 || terrain_type >= terrain_types.size() || !terrain_types[terrain_type].IsLoaded())
	{
		return OPTI_TERRAIN_GRAMMAR_NON_EXISTENT;
	}
	
	auto new_type = opti::ProceduralTerrainType();
	
	try
	{
		new_type.LoadData(grammar, std::strlen(grammar));
		terrain_types[terrain_type].UnLoad();
		terrain_types[terrain_type] = new_type;
		return terrain_type;
	}
	catch (opti::ParseException &e)
	{
		last_grammar_error.error.line = e.line;
		std::strncpy(last_grammar_error.error.message, e.message.c_str(), sizeof(last_grammar_error.error.message));
		last_grammar_error.error.message[sizeof(last_grammar_error.error.message) - 1] = '\0';
		
		return OPTI_TERRAIN_GRAMMAR_ERROR;
	}
}

opti_TerrainGrammarError* opti_getLastError(void)
{
	return &last_grammar_error.error;
}

int opti_freeTerrainType(int terrain_type)
{
	if (terrain_type >= terrain_types.size() || terrain_type < 0)
	{
		return OPTI_TERRAIN_GRAMMAR_NON_EXISTENT;
	}
	
	terrain_types[terrain_type].UnLoad();
	
	return OPTI_TERRAIN_SUCCESS;
}

int opti_generateSingleTerrain(int terrain_type, int x, int y, int w, int h, int lod, float* heights, int* textures)
{
	if (terrain_type >= terrain_types.size() || terrain_type < 0) { return OPTI_TERRAIN_INVALID_TERRAIN_TYPE; }
	if (w < 2) { return OPTI_TERRAIN_INVALID_WIDTH; }
	if (h < 2) { return OPTI_TERRAIN_INVALID_HEIGHT; }
	if (!opti::IntegerMaths::IsPowerOfTwo(lod) || lod < 1) { return OPTI_TERRAIN_INVALID_LOD; }
	
	opti::TerrainData terrain(w, h, heights, textures);
	opti::ProceduralTerrainType* type = terrain_type >= 0 && terrain_type < terrain_types.size() ? &terrain_types[terrain_type] : &terrain_types[0];
	opti::ProceduralTerrainSource source;
	source.GenerateSingleTerrainType(type, x / lod, y / lod, lod, &terrain);

	return OPTI_TERRAIN_SUCCESS;
}

int opti_generateMultiTerrain(opti_MapCallback callback, void* user_data, int map_scale, int x, int y, int w, int h, int lod, float* heights, int* textures)
{
	if (w < 2) { return OPTI_TERRAIN_INVALID_WIDTH; }
	if (h < 2) { return OPTI_TERRAIN_INVALID_HEIGHT; }
	if (!opti::IntegerMaths::IsPowerOfTwo(lod) || lod < 1) { return OPTI_TERRAIN_INVALID_LOD; }
	if (map_scale / lod < 2) { return OPTI_TERRAIN_INVALID_LOD_AND_SCALE; }
	
	opti::TerrainData terrain(w, h, heights, textures);
	opti::ProceduralTerrainSource source;
	source.GenerateMultiTerrainType(callback, user_data, map_scale, terrain_types, x / lod, y / lod, lod, &terrain);
	
	return OPTI_TERRAIN_SUCCESS;
}
