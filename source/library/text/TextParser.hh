/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
#include <set>
#include <vector>
#include "library/text/ParseException.hh"
#include "library/text/ITokenizer.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Tokenizes by whitespace. Multiple whitespace characters separate tokens.
 */
class WhitespaceTokenizer : public ITokenizer {
public:
	void SetData(const char *str, int length);
	bool GetToken(Token *token);
private:
	const char *data;
	int dataLength;
	int dataPosition;
};

/**
 * Tokenizes by whitespace, except when the whitespace is between double-quotes
 * on the same line. In other words when reading character-by-character, the
 * first double quote disables whitespace from breaking a token, and the next
 * enables it again, and so on. A newline character resets this.
 */
class QuotedStringsTokenizer : public ITokenizer {
public:
	void SetData(const char *str, int length);
	bool GetToken(Token *token);
private:
	const char *data;
	int dataLength;
	int dataPosition;
};

// TODO: Multi-char string openers and closers, allowing different closer.
// TODO: Custom breaking non-tokens (e.g. whitespace)
class CustomWhitespaceTokenizer : public ITokenizer {
public:
	void SetData(const char *str, int length);
	bool GetToken(Token *token);

	void AddBreakingToken(std::string token) { breakingTokens.insert(token); }
	void AddStringEncloser(char encloser) { stringEnclosers.insert(encloser); }
private:
	const char *data;
	int dataLength;
	int dataPosition;

	std::set<std::string> breakingTokens;
	std::set<char> stringEnclosers;

	int AtBreakingToken();
};

/**
 * A utility for parsing text, reading off individual tokens or whole lines.
 * Throws exceptions for erroneous data so enabling actual file parsing code
 * to be very tidy.
 */
class TextParser {
public:
	TextParser() : currentToken(0), currentLine(0) { }

	/**
	 * Constructs with the given data. The parser points into the data without
	 * making a copy and so user code must ensure the data persists while the
	 * object is in use.
	 * The token and line pointers are set to point to the very first token.
	 * @param str The character string data.
	 * @param dataLength Length of the data enabling the string to omit a null
	 *                   terminator.
	 * @param tokenizer Specifies the tokenizing rules to be used.
	 */
	TextParser(const char *str, int dataLength, ITokenizer &tokenizer, bool includeNewlines = false);

	/**
	 * Returns the next token, throwing ParseException if the end of the file
	 * was reached.
	 */
	std::string GetToken();
	/**
	 * Reads and discards the next token, but throws ParseException if it does
	 * not match the specified value.
	 */
	void ExpectToken(std::string t);
	/**
	 * Throws ParseException if we have not read all tokens, else does nothing.
	 */
	void ExpectEof();
	/**
	 * Returns true if we have read all tokens, else false.
	 */
	bool Eof();
	/**
	 * Returns a new TextParser object which contains tokens from the current
	 * token to the end of the line, advancing the current object's pointer to
	 * the start of the next line.
	 * Throws ParseException if we have already read all tokens.
	 */
	TextParser GetLine();

	int NumLines();
	int NumTokens();

	/**
	 * Throws a ParseException with a custom message with its line and token
	 * set to our current position.
	 */
	void ThrowCustomParseException(std::string m);

	/**
	 * Get the current line number that we're parsing.
	 */
	int GetCurrentLineNumber();

private:
	const char *data;

	std::vector<Token> tokens;

	struct Line {
		int lineNumber;
		int start;
		int length;
		Line(int num, int s, int l) : lineNumber(num), start(s), length(l) { }
		~Line() { }
	};
	std::vector<Line> lines;

	int currentToken;
	int currentLine;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
