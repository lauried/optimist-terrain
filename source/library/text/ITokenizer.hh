/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <string>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

/**
 * Data about a token within a specific string.
 */
struct Token {
	int offset; ///< Offset within the string.
	int length; ///< Number of characters length of the token.
	int line, column; ///< Line and column within the file (optional)
	const char *buffer; ///< Pointer to the start of the token within the buffer (optional)
	std::string typeId; ///< Tokenizer specific identifier as to the token type.

	Token() :
		offset(0), length(0), line(0), column(0), buffer(nullptr) {}

	Token(int ofs, int len) :
		offset(ofs), length(len), line(0), column(0), buffer(nullptr) {}

	Token(const char *buf, int ofs, int len, int line, int col, std::string type) :
		offset(ofs), length(len), line(line), column(col), buffer(buf), typeId(type) {}

	Token(const Token& other) :
		offset(other.offset), length(other.length), line(other.line), column(other.column),
		buffer(other.buffer), typeId(other.typeId) {}

	void operator=(const Token &other)
	{
		offset = other.offset;
		length = other.length;
		line = other.line;
		column = other.column;
		buffer = other.buffer;
		typeId = other.typeId;
	}

	/**
	 * If the buffer is present, returns the string length of the token.
	 */
	int size() const { return (buffer != nullptr) ? length : 0; }

	/**
	 * If the buffer is present, returns the character of the token at the
	 * given index.
	 */
	char at(int index) const { return (buffer != nullptr) ? buffer[index] : '\0'; }

	/**
	 * Returns the token as a std::string.
	 */
	std::string str() const { return (buffer != nullptr) ? std::string(buffer, length) : std::string(); }
};

/**
 * Interface for breaking up text into tokens according to impelementation
 * -specific tokenizing rules.
 */
class ITokenizer {
public:
	/**
	 * Sets the data to be parsed, and sets the current token to the very first.
	 * The tokenizer never copies the data, it is the responsibility of the
	 * user code to ensure the data persists while the tokenizer is being used.
	 * @param str A character array of data to be processed.
	 * @param length The length of the character array specified by str,
	                 allowing the array to not be null terminated.
	 */
	virtual void SetData(const char *str, int length) = 0;
	/**
	 * Tries to retrieve the next token.
	 * If there are no more tokens to be read it will return false,
	 * otherwise it will return true and write to the passed structure.
	 * @param token The Token struct to which the data will be written.
	 * @return False if a token was not read, else true.
	 */
	virtual bool GetToken(Token *token) = 0;
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
