/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#include "library/text/TextParser.hh"
#include <cstring>
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

static int CountNewlines(const char *str, int length)
{
	int countN = 0;
	int countR = 0;
	for (int i = 0; i < length; i += 1)
	{
		if (str[i] == '\n')
		{
			countN += 1;
		}
		if (str[i] == '\r')
		{
            countR += 1;
		}
	}
	if (countN > countR)
	{
        return countN;
    }
    return countR;
}

TextParser::TextParser(const char *str, int dataLength, ITokenizer &tokenizer, bool includeNewlines)
{
	data = str;
	tokenizer.SetData(data, dataLength);

	int lineNumber = 0;
	lines.push_back(Line(lineNumber, -1, 0));
	Line *line = &lines[lines.size() - 1];

	Token token;
	while (tokenizer.GetToken(&token))
	{
#if 0
		char buffer[token.length + 1];
		strncpy(buffer, &data[token.offset], token.length);
		buffer[token.length] = '\0';
		std::cout << "read token '" << buffer << "'" << std::endl;
#endif

		int newlines = CountNewlines(&data[token.offset], token.length);

		if (token.length == 0 || data[token.offset] == '\0')
		{
		}
		else if (token.length == 1 && data[token.offset] == '\n')
		{
			lineNumber += 1;
			lines.push_back(Line(lineNumber, -1, 0));
			line = &lines[lines.size() - 1];
			if (includeNewlines)
			{
				tokens.push_back(token);
			}
		}
		else if (newlines > 0)
		{
			lineNumber += newlines;
			lines.push_back(Line(lineNumber, -1, 0));
			line = &lines[lines.size() - 1];
			if (includeNewlines)
			{
				tokens.push_back(token);
			}
		}
		else
		{
			tokens.push_back(token);
			if (line->start == -1)
			{
				line->start = tokens.size() - 1;
            }
			line->length += 1;
		}
	}

	// Initialize token and line.
	// CurrentLine is the first line with a token on it.
	currentToken = 0;
	for (currentLine = 0; currentLine < lines.size(); ++currentLine)
	{
		if (lines[currentLine].start > -1)
		{
			break;
        }
	}
	if (currentLine >= lines.size())
	{
		throw ParseException(0, 0, "No data in file");
    }
}

std::string TextParser::GetToken()
{
	// If the token's at the end, throw eof.
	if (currentToken >= tokens.size())
	{
		throw ParseException(GetCurrentLineNumber(), currentToken, "End of file reached reading token");
    }

	int tokenLength = tokens[currentToken].length;
	std::vector<char> buffer(tokenLength + 1);
	strncpy(&buffer[0], &data[tokens[currentToken].offset], tokenLength);
	buffer[tokenLength] = '\0';

	// Increment token pointer.
	++currentToken;

	// Check if we should update the line pointer.
	if (currentToken >= lines[currentLine].start + lines[currentLine].length)
	{
		++currentLine;
    }

	return std::string(&buffer[0]);
}

void TextParser::ExpectToken(std::string t)
{
	std::string got = GetToken();
	if (t.compare(got))
	{
		throw ParseException(GetCurrentLineNumber(), currentToken, "Expected token '" + t + "', got '" + got + "'");
    }
}

void TextParser::ExpectEof()
{
	if (currentToken != tokens.size())
	{
		throw ParseException(GetCurrentLineNumber(), currentToken, "Expected end of file or line");
    }
}

bool TextParser::Eof()
{
	return (currentToken >= tokens.size());
}

TextParser TextParser::GetLine()
{
	if (currentLine >= lines.size())
		throw ParseException(GetCurrentLineNumber(), currentToken, "End of file reached reading line");

	// Build parser containint next line.
	TextParser parser;
	parser.data = data;
	parser.currentToken = 0;
	parser.currentLine = 0;
	if (lines[currentLine].start >= 0)
	{
		int posInLine = lines[currentLine].start - currentToken;
		for (int i = posInLine; i < lines[currentLine].length; ++i)
		{
			parser.tokens.push_back(tokens[lines[currentLine].start + i]);
		}
		parser.lines.push_back(Line(lines[currentLine].lineNumber, 0, lines[currentLine].length - posInLine));
	}

	// Increase line pointer and token pointer to start of next line.
	++currentLine;
	while (currentLine < lines.size() && lines[currentLine].start == -1)
	{
		++currentLine;
	}
	if (currentLine < lines.size())
		currentToken = lines[currentLine].start;
	else
		currentToken = tokens.size();

	return parser;
}

int TextParser::NumLines()
{
	return lines.size();
}

int TextParser::NumTokens()
{
	return tokens.size();
}

void TextParser::ThrowCustomParseException(std::string m)
{
	throw ParseException(GetCurrentLineNumber(), currentToken, m);
}

int TextParser::GetCurrentLineNumber()
{
	if (currentLine < lines.size())
		return lines[currentLine].lineNumber + 1;
	if (lines.size() > 0)
		return lines[lines.size() - 1].lineNumber + 1;
	return -1;
}

//-----------------------------------------------------------------------------
// WhitespaceParser
//-----------------------------------------------------------------------------

void WhitespaceTokenizer::SetData(const char *str, int length)
{
	data = str;
	dataLength = length;
	dataPosition = 0;
}

bool WhitespaceTokenizer::GetToken(Token *token)
{
	if (dataPosition >= dataLength)
		return false;

	// advance to first non-whitespace (where \n isn't whitespace)
	while (dataPosition < dataLength
		&& (
			data[dataPosition] == ' '
			|| data[dataPosition] == '\t'
			|| data[dataPosition] == '\r'
			)
		)
	{ ++dataPosition; }

	// if we reached eof before whitespace, return false
	if (dataPosition >= dataLength)
		return false;

	// if we reached a newline, advance pointer by one and return the token
	if (data[dataPosition] == '\n')
	{
		token->offset = dataPosition;
		token->length = 1;
		++dataPosition;
		return true;
	}

	// mark the start of the token and advance to the end
	int tokenStart = dataPosition;
	while (dataPosition < dataLength
		&& data[dataPosition] != ' '
		&& data[dataPosition] != '\t'
		&& data[dataPosition] != '\r'
		&& data[dataPosition] != '\n'
		)
	{
		++dataPosition;
	}

	token->offset = tokenStart;
	token->length = dataPosition - tokenStart;
	return true;
}

//-----------------------------------------------------------------------------
// QuotedStringsTokenizer
//-----------------------------------------------------------------------------

void QuotedStringsTokenizer::SetData(const char *str, int length)
{
	data = str;
	dataLength = length;
	dataPosition = 0;
}

bool QuotedStringsTokenizer::GetToken(Token *token)
{
	if (dataPosition >= dataLength)
		return false;

	// advance to first non-whitespace (where \n isn't whitespace)
	while (dataPosition < dataLength
		&& (
			data[dataPosition] == ' '
			|| data[dataPosition] == '\t'
			|| data[dataPosition] == '\r'
			)
		)
	{ ++dataPosition; }

	// if we reached eof before non-whitespace, return false
	if (dataPosition >= dataLength)
		return false;

	// if we reached a newline, advance pointer by one and return the token
	if (data[dataPosition] == '\n')
	{
		token->offset = dataPosition;
		token->length = 1;
		++dataPosition;
		return true;
	}

	// if we reached a ", continue to the next one, or a newline or eof
	if (data[dataPosition] == '"')
	{
		int tokenStart = dataPosition;
		++dataPosition;
		while (dataPosition < dataLength
			&& data[dataPosition] != '"'
			&& data[dataPosition] != '\r'
			&& data[dataPosition] != '\n'
			)
		{
			++dataPosition;
		}
		if (dataPosition < dataLength && data[dataPosition] == '"')
			++dataPosition;

		token->offset = tokenStart;
		token->length = dataPosition - tokenStart;
		return true;
	}

	// mark the start of the token and advance to the end
	int tokenStart = dataPosition;
	while (dataPosition < dataLength
		&& data[dataPosition] != ' '
		&& data[dataPosition] != '\t'
		&& data[dataPosition] != '\r'
		&& data[dataPosition] != '\n'
		)
	{
		++dataPosition;
	}

	token->offset = tokenStart;
	token->length = dataPosition - tokenStart;
	return true;
}

//-----------------------------------------------------------------------------
// CustomWhitespaceTokenizer
//-----------------------------------------------------------------------------

void CustomWhitespaceTokenizer::SetData(const char *str, int length)
{
	data = str;
	dataLength = length;
	dataPosition = 0;
}

// Returns the length of the breaking token if we're currently at one, else
// zero.
int CustomWhitespaceTokenizer::AtBreakingToken()
{
	int remaining = dataLength - dataPosition;
	for (auto it = breakingTokens.begin(); it != breakingTokens.end(); ++it)
	{
		if (it->length() > remaining)
			continue;

		bool equal = true;
		for (int i = 0; i < it->length(); ++i)
		{
			if ((*it)[i] != data[dataPosition + i])
			{
				equal = false;
				break;
			}
		}
		if (equal)
			return it->length();
	}
	return 0;
}

bool CustomWhitespaceTokenizer::GetToken(Token *token)
{
	if (dataPosition >= dataLength)
		return false;

	// advance to first non-whitespace (where \n isn't whitespace)
	while (dataPosition < dataLength
		&& (
			data[dataPosition] == ' '
			|| data[dataPosition] == '\t'
			|| data[dataPosition] == '\r'
			)
		)
	{ ++dataPosition; }

	// if we reached eof before non-whitespace, return false
	if (dataPosition >= dataLength)
		return false;

	// if we reached a newline, advance pointer by one and return the token
	if (data[dataPosition] == '\n')
	{
		token->offset = dataPosition;
		token->length = 1;
		++dataPosition;
		return true;
	}

	// check for custom breaking tokens - they work like '\n' but longer
	int breakingTokenLength = AtBreakingToken();
	if (breakingTokenLength)
	{
		token->offset = dataPosition;
		token->length = breakingTokenLength;
		dataPosition += breakingTokenLength;
		return true;
	}

	// if we reached a ", continue to the next one, or a newline or eof
	auto it = stringEnclosers.find(data[dataPosition]);
	if (it != stringEnclosers.end())
	{
		char stringChar = *it;
		int tokenStart = dataPosition;
		++dataPosition;
		while (dataPosition < dataLength
			&& data[dataPosition] != stringChar
			&& data[dataPosition] != '\r'
			&& data[dataPosition] != '\n'
			)
		{
			++dataPosition;
		}
		if (dataPosition < dataLength && data[dataPosition] == stringChar)
			++dataPosition;

		token->offset = tokenStart;
		token->length = dataPosition - tokenStart;
		return true;
	}

	// mark the start of the token and advance to the end
	int tokenStart = dataPosition;
	while (dataPosition < dataLength
		&& data[dataPosition] != ' '
		&& data[dataPosition] != '\t'
		&& data[dataPosition] != '\r'
		&& data[dataPosition] != '\n'
		&& !AtBreakingToken()
		)
	{
		++dataPosition;
	}

	token->offset = tokenStart;
	token->length = dataPosition - tokenStart;
	return true;
}

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

