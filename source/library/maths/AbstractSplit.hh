/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <iostream>
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class AbstractSplit {
protected:
	inline int PosMod(int a, int n)
	{
		int mod = a % n;
		if ( mod < 0)
		{
			mod += n;
		}
		return mod;
	}

	inline float CurrentFraction(int pos, int grid)
	{
		return ((float)PosMod(pos, grid) / (float)grid);
	}

	inline void NextPosAndFraction(int current, int grid, int end, int * const next, float * const fraction)
	{
		*fraction = 1.0f;
		current += grid;
		current -= PosMod(current, grid);
		if (current > end)
		{
			current = end;
			*fraction = CurrentFraction(current, grid);
		}
		*next = current;
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------

