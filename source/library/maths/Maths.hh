/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

//-----------------------------------------------------------------------------
#pragma once
//-----------------------------------------------------------------------------
#include <cmath>
#include <cstdlib>

#ifndef M_PI
#define M_PI 3.14159265358979323846264
#endif

#ifndef M_E
#define M_E  2.71828182845904523560287
#endif

#include "library/geometry/Vector3.hh"
//-----------------------------------------------------------------------------
namespace opti {
//-----------------------------------------------------------------------------

class Maths {
public:
	/**
	 * Returns true if the integer is a power of two, else false.
	 */
	static bool IsPowerOfTwo(int i)
	{
		return i && !(i & (i - 1));
	}

	/**
	 * Convert degrees to radians.
	 */
	template<typename T>
	static T DegreesToRadians(T a)
	{
		return a * (M_PI / 180.0);
	}

	/**
	 * Convert radians to degrees.
	 */
	template<typename T>
	static T RadiansToDegrees(T a)
	{
		return a * (180.0 / M_PI);
	}

	/**
	 * Convert Tau (rotations) to radians.
	 */
	template<typename T>
	static T TauToRadians(T a)
	{
		return a * M_PI * 2.0;
	}

	/**
	 * Convert raidans to Tau (rotations).
	 */
	template<typename T>
	static T RadiansToTau(T a)
	{
		return a / (M_PI * 2.0);
	}

	/**
	 * Essentially just a macro for squaring a value. It's used for squaring
	 * an expression inline without requiring an explicit variable to store it.
	 * Just syntactic sugar, for code clarity.
	 */
	template<typename T>
	static T Square(T s)
	{
		return s * s;
	}

	/**
	 * Returns the value clamped between the minimum and maximum (inclusive).
	 */
	template<typename T>
	static T Bound(T min, T val, T max)
	{
		if (val < min) return min;
		if (val > max) return max;
		return val;
	}

	/**
	 * Wraps a value within a given range.
	 */
	template<typename T>
	static T Wrap(T min, T val, T max)
	{
		return min + (fmod(val - min, max - min));
	}

	/**
	 * Returns true if a line intersects a sphere, and sets the distances along
	 * the line at which intersections occur. Both distances will be the same
	 * if the line just touches the sphere. They will not be updated if the
	 * line does not touch the sphere at all.
	 * @param sphereOrigin
	 * @param sphereRadius
	 * @param lineP1 the Starting point for the line.
	 * @param lineDirection the direction of the line, which needn't be
	 *        normalized.
	 * @param distance1 Set to a distance if there's an intersection.
	 * @param distance2 Set to the other distance if there's an intersection.
	 */
	template<typename T>
	static bool LineSphereIntersection(
		const Vector3<T> &sphereOrigin, const T sphereRadius,
		const Vector3<T> &lineP1, const Vector3<T> &lineDirection,
		T* distance1, T* distance2)
	{
		Vector3<T> relativeSphereOrigin = sphereOrigin - lineP1;
		Vector3<T> direction = lineDirection.GetNormalized();
		T rhs = Square(direction.DotProduct(relativeSphereOrigin))
					 - relativeSphereOrigin.Length()
					 + Square(sphereRadius);
		if (rhs < 0)
			return false;
		T lhs = direction.DotProduct(relativeSphereOrigin);
		rhs = sqrt(rhs);
		*distance1 = lhs - rhs;
		*distance2 = lhs + rhs;
		return true;
	}

	/**
	 * Find the closest point to another point, on a line.
	 */
	template<typename T>
	static Vector3<T> ClosestPointOnLine(const Vector3<T> &toPoint, const Vector3<T> &lineP1, const Vector3<T> &lineDirection)
	{
		Vector3<T> direction = lineDirection.GetNormalized();
		T distance = direction.DotProduct(toPoint - lineP1);
		return lineP1 + direction * distance;
	}
};

//-----------------------------------------------------------------------------
} // namespace opti
//-----------------------------------------------------------------------------
