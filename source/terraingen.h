/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

#ifndef __OPTIMIST_TERRAINGEN_H__
#define __OPTIMIST_TERRAINGEN_H__

#if defined(__cplusplus) || defined(c_plusplus)
extern "C" {
#endif

#ifdef COMPILE_LIBRARY
#  define EXPORT_PARAMS __attribute__((__visibility__("default")))
#  ifdef __cplusplus
#    define LIB_EXPORT EXPORT_PARAMS
#  else
#    define LIB_EXPORT export EXPORT_PARAMS
#  endif
#else
#  define LIB_EXPORT
#endif

/**
 * Glossary/concepts:
 *
 * tiles
 * Squares/cells on the output heightmap, a combination of heightmap samples
 * and tile types.
 *
 * texture
 * An abstract identifier for texture or other surface properties. This library
 * doesn't care about their meaning.
 *
 * chunk
 * A rectangle of tiles.
 *
 * terrain type
 * A particular style of generating chunks, usually corresponding to a
 * different type of terrain, e.g. plains, mountains, cliffs.
 *
 * terrain map
 * A large scale map saying what terrain type is used where. One cell of a map
 * corresponds to some number of tiles.
 *
 * Conventions:
 * x, y, w, h
 * X and y are the lowest x and y coordinates, w and h are the size in x and y
 * respectively.
 *
 * buffers
 * A buffer always has room for the given w*h. They are stored such that they
 * can be indexed by [y*w+x].
 */

const int OPTI_TERRAIN_SUCCESS               =  0;
const int OPTI_TERRAIN_GRAMMAR_ERROR         = -1;
const int OPTI_TERRAIN_GRAMMAR_NON_EXISTENT  = -2;
const int OPTI_TERRAIN_INVALID_WIDTH         = -3;
const int OPTI_TERRAIN_INVALID_HEIGHT        = -4;
const int OPTI_TERRAIN_INVALID_LOD_AND_SCALE = -5;
const int OPTI_TERRAIN_INVALID_LOD           = -6;
const int OPTI_TERRAIN_INVALID_TERRAIN_TYPE  = -7;

/**
 * Parses a terrain type from the given string using the defined grammar.
 * Returns OPTI_TERRAIN_GRAMMAR_ERROR if a parse error occurred,
 * otherwise the handle for the terrain type.
 */
LIB_EXPORT int opti_registerTerrainType(const char* grammar);

/**
 * Loads a new terrain grammar into a previously registered terrain grammar.
 * If a parse error occured, OPTI_TERRAIN_GRAMMAR_ERROR is returned and the
 * existing terrain grammar remains in place.
 * If the handle passed in to terrain_type is not registered or has been freed,
 * returns OPTI_TERRAIN_GRAMMAR_NON_EXISTENT.
 * Otherwise, returns the value passed in to terrain_type.
 */
LIB_EXPORT int opti_updateTerrainType(const char* grammar, int terrain_type);

typedef struct {
	int line;
	char message[512];
} opti_TerrainGrammarError;

/**
 * Returns details on the last error that occurred from registerTerrainType.
 */
LIB_EXPORT opti_TerrainGrammarError* opti_getLastError(void);

/**
 * De-allocates a terrain type.
 * @returns OPTI_TERRAIN_SUCCESS or OPTI_TERRAIN_GRAMMAR_NON_EXISTENT.
 */
LIB_EXPORT int opti_freeTerrainType(int terrain_type);

/**
 * Generate a piece of a single terrain type, writing it into the provided
 * buffers. Use of an un-registered or freed terrain type is undefined.
 * LOD must be a power of 2.
 * @returns OPTI_TERRAIN_SUCCESS or an OPTI_TERRAIN_* error.
 */
LIB_EXPORT int opti_generateSingleTerrain(
	int terrain_type,
	int x,
	int y,
	int w,
	int h,
	int lod,
	float* heights,
	int* textures
);

/**
 * Coordinates are specified in map cells, NOT tiles.
 * The terrain types must be those returned by registerTerrainType.
 * The coarse heights are a large scale heightmap which can be used to generate
 * huge mountains and so on.
 * The user data is that which was passed in to generateMultiTerrain, and can
 * be used to refer to a particular map.
 */
typedef void (*opti_MapCallback)(
	void* user_data,
	int x,
	int y,
	int w,
	int h,
	int* terrain_types,
	float *coarse_heights
);

/**
 * Generates terrain by blending different terrain types.
 * The callback must provide terrain types for the required dimensions.
 * You can use this library to generate that map too, if you like.
 * Use of un-registered or freed terrain types is undefined.
 * LOD must be a power of 2, and map_scale/LOD must be 2 or greater.
 * @returns OPTI_TERRAIN_SUCCESS or an OPTI_TERRAIN_* error.
 */
LIB_EXPORT int opti_generateMultiTerrain(
	opti_MapCallback callback,
	void* user_data,
	int map_scale,
	int x,
	int y,
	int w,
	int h,
	int lod,
	float* heights,
	int* textures
);

#if defined(__cplusplus) || defined(c_plusplus)
} // extern "C"
#endif

#endif
