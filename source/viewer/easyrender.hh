/*
===============================================================================
Optimist Terrain Generation
A library for procedurally generating terrain heightmaps.
Copyright © 2010-2022 David Laurie
===============================================================================
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
===============================================================================
*/

/*
===============================================================================
Single-file easy OpenGL rendering library.
This is meant to be easy to drop in to an application to render stuff without
figuring out the entire rendering checklist each time, for the times you don't
want a full framework. Some day you'll want to render better but this should
get you something on screen for the here and now.

Usage:
  Renderer renderer;
  renderer.Init();

Then, before drawing a scene:
  renderer.SetViewport(...);
  Orientation<float> orientation;
  // use operations on orientation to position it how you want
  renderer.SetCamera(...);
  
Then, to draw a scene:
  Vertex verts[size];
  // write to your verts
  renderer.drawMesh(verts, size);

The user is still responsible for some other OpenGL stuff like loading the
library and swapping buffers.
===============================================================================
*/

#include <cstring>
#include <cmath>
#include <iostream>

//-----------------------------------------------------------------------------
// Stuff
//-----------------------------------------------------------------------------

template <typename T> inline T DegToRad(T deg)
{
	return deg * 3.1415926535 / 180.0;
}

//-----------------------------------------------------------------------------
// Vector Maths
//-----------------------------------------------------------------------------

struct Vec3
{
	float p[3];
	
	Vec3()
	{
		p[0] = p[1] = p[2] = 0;
	}
	
	Vec3(float x, float y, float z)
	{
		p[0] = x;
		p[1] = y;
		p[2] = z;
	}
	
	Vec3 operator+(Vec3 a)
	{
		return Vec3(p[0] + a.p[0], p[1] + a.p[1], p[2] + a.p[2]);
	}
	
	Vec3 operator-(Vec3 a)
	{
		return Vec3(p[0] - a.p[0], p[1] - a.p[1], p[2] - a.p[2]);
	}
};

template <typename T> T Length(const T* v)
{
	return std::sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
}

template <typename T> T Normalize(const T* v, T* result)
{
	T length = Length<T>(v);
	T scale = 1.0 / length;
	result[0] = v[0] * scale;
	result[1] = v[1] * scale;
	result[2] = v[2] * scale;
	return length;
}

template <typename T> float DotProduct(const T* a, const T* b)
{
	return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

/**
 * '1 0 0' × '0 1 0' = '0 0 1'
 */
template <typename T> void CrossProduct(const T* a, const T* b, T* result)
{
	result[0] = a[1] * b[2] - a[2] * b[1];
	result[1] = a[2] * b[0] - a[0] * b[2];
	result[2] = a[0] * b[1] - a[1] * b[0];
}

//-----------------------------------------------------------------------------
// Orientation
//-----------------------------------------------------------------------------

template <typename T> class Orientation
{
public:
	T forward[3];
	T right[3];
	T up[3];
	T position[3];

	/**
	 * Returns an orientation whose axes match world space.
	 */
	static Orientation<T> identity()
	{
		Orientation ori = {
			{ 1, 0, 0 }, // forward
			{ 0, 1, 0 }, // right
			{ 0, 0, 1 }, // up
			{ 0, 0, 0 }, // position
		};
		return ori;
	}

	static Orientation<T> fromPosition(float* position)
	{
		auto ori = Orientation<T>::identity();
		ori.position[0] = position[0];
		ori.position[1] = position[1];
		ori.position[2] = position[2];
		return ori;
	}

	static Orientation<T> fromPositionEuler(float* position, float* euler)
	{
		auto ori = Orientation<T>::fromPosition(position);
		ori.rotateZ(euler[2]);
		ori.rotateY(euler[1]);
		ori.rotateX(euler[0]);
		return ori;
	}

	/**
	 * Rotate clockwise looking along Z axis or, if you could grab +Z, twiddle it counterclockwise.
	 */
	void rotateZ(T angle)
	{
		const T s = std::sin(angle);
		const T c = std::cos(angle);
		const T newForward[3] = {
			forward[0] * c - right[0] * s,
			forward[1] * c - right[1] * s,
			forward[2] * c - right[2] * s,
		};
		Normalize<T>(newForward, forward);
		CrossProduct<T>(up, forward, right);
		Normalize<T>(right, right);
	}

	/**
	 * Rotate clockwise looking along Y axis or, if you could grab +Y, twiddle it counterclockwise.
	 */
	void rotateY(T angle)
	{
		const T s = std::sin(angle);
		const T c = std::cos(angle);
		const T newForward[3] = {
			forward[0] * c + up[0] * s,
			forward[1] * c + up[1] * s,
			forward[2] * c + up[2] * s,
		};
		Normalize<T>(newForward, forward);
		CrossProduct<T>(forward, right, up);
		Normalize<T>(up, up);
	}

	/**
	 * Rotate clockwise looking along X axis or, if you could grab +X, twiddle it counterclockwise.
	 */
	void rotateX(T angle)
	{
		const T s = std::sin(angle);
		const T c = std::cos(angle);
		const T newRight[3] = {
			right[0] * c - up[0] * s,
			right[1] * c - up[1] * s,
			right[2] * c - up[2] * s,
		};
		Normalize<T>(newRight, right);
		CrossProduct<T>(forward, right, up);
		Normalize<T>(up, up);
	}

	/**
	 * Move in a direction in the orientation's space.
	 */
	void moveRelative(T direction[3])
	{
		position[0] += (forward[0] * direction[0]) + (right[0] * direction[1]) + (up[0] * direction[2]);
		position[1] += (forward[1] * direction[0]) + (right[1] * direction[1]) + (up[1] * direction[2]);
		position[2] += (forward[2] * direction[0]) + (right[2] * direction[1]) + (up[2] * direction[2]);
	}
};

//-----------------------------------------------------------------------------
// Matrix
//-----------------------------------------------------------------------------

template <typename T> class Matrix
{
public:
	// Addressed [row][column]
	T m[4][4];

	static Matrix<T> zero()
	{
		Matrix<T> mat = {
			{
				{ 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0 },
				{ 0.0, 0.0, 0.0, 0.0 },
			}
		};
		return mat;
	}

	static Matrix<T> identity()
	{
		Matrix<T> mat = {
			{
				{ 1.0, 0.0, 0.0, 0.0 },
				{ 0.0, 1.0, 0.0, 0.0 },
				{ 0.0, 0.0, 1.0, 0.0 },
				{ 0.0, 0.0, 0.0, 1.0 },
			}
		};
		return mat;
	}

	/**
	 * Takes the orientation of a model.
	 * Returns a matrix that transforms from model space into world space.
	 * It's the complement of a camera matrix.
	 */
	static Matrix<T> model(const T* forward, const T* right, const T* up, const T* origin)
	{
		Matrix<T> mat = {
			{
				{ forward[0], forward[1], forward[2], 0.0 },
				{ right[0],   right[1],   right[2],   0.0 },
				{ up[0],      up[1],      up[2],      0.0 },
				{ origin[0],  origin[1],  origin[2],  1.0 },
			}
		};
		return mat;
	}

	/**
	 * Takes the orientation of a camera in world space.
	 * Returns a matrix that transforms from world space to camera space.
	 * It's the complement of a model matrix.
	 */
	static Matrix<T> camera(const T* forward, const T* right, const T* up, const T* origin)
	{
		Matrix<T> translate = {
			{
				{ 1.0, 0.0, 0.0, 0.0 },
				{ 0.0, 1.0, 0.0, 0.0 },
				{ 0.0, 0.0, 1.0, 0.0 },
				{ -origin[0], -origin[1], -origin[2], 1.0 },
			}
		};

		Matrix<T> rotate = {
			{
				{ forward[0], right[0], up[0], 0.0 },
				{ forward[1], right[1], up[1], 0.0 },
				{ forward[2], right[2], up[2], 0.0 },
				{ 0.0, 0.0, 0.0, 1.0 },
			}
		};

		return rotate.multiply(translate);
	}

	// Document style orthographic, where 0, 0 is top left.
	static Matrix<T> orthographicDocument(T left, T right, T top, T bottom, T near, T far)
	{
		T midX = (left + right) / 2;
		T midY = (top + bottom) / 2;
		T midZ = (near + far) / 2;

		T scaleX = 2.0 / (right - left);
		T scaleY = 2.0 / (bottom - top);
		T scaleZ = 2.0 / (far - near);

		Matrix<T> ortho = {
			{
				{ scaleX, 0, 0, 0 },
				{ 0, -scaleY, 0, 0 },
				{ 0, 0, scaleZ, 0 },
				{ -1.0f, 1.0f, 0, 1 },
			}
		};

		return ortho;
	}
	
	static Matrix<T> perspective(T hFov, T vFov, T nearDist, T farDist)
	{
		T depth = farDist - nearDist;
		T ihFov = 1 / hFov;
		T ivFov = 1 / vFov;
		T fn1 = -(farDist + nearDist) / depth;
		T fn2 = (-2 * farDist * nearDist) / depth;

		Matrix<T> mat = {
			{
				{ ihFov, 0.0,   0.0,  0.0 },
				{ 0.0,   ivFov, 0.0,  0.0 },
				{ 0.0,   0.0,   fn1, -1.0 },
				{ 0.0,   0.0,   fn2,  0.0 },
			}
		};

		return mat;
	}

	static Matrix<T> reversePerspective(T hFov, T vFov, T nearDist, T farDist)
	{
		T depth = farDist - nearDist;
		T ihFov = 1 / hFov;
		T ivFov = 1 / vFov;
		T fn1 = nearDist / depth;
		T fn2 = (farDist * nearDist) / depth;

		Matrix<T> mat = {
			{
				{ ihFov, 0.0,   0.0,  0.0 },
				{ 0.0,   ivFov, 0.0,  0.0 },
				{ 0.0,   0.0,   fn1, -1.0 },
				{ 0.0,   0.0,   fn2,  0.0 },
			}
		};

		return mat;
	}

	Matrix<T> transpose()
	{
		Matrix<T> dest = {
			{
				{ m[0][0], m[1][0], m[2][0], m[3][0] },
				{ m[0][1], m[1][1], m[2][1], m[3][1] },
				{ m[0][2], m[1][2], m[2][2], m[3][2] },
				{ m[0][3], m[1][3], m[2][3], m[3][3] },
			}
		};

		return dest;
	}

	Matrix<T> multiply(const Matrix<T>& b)
	{
		Matrix<T> dest = zero();

		for (auto dr = 0; dr < 4; dr += 1) {
			for (auto dc = 0; dc < 4; dc += 1) {
				for (auto i = 0; i < 4; i += 1) {
					dest.m[dr][dc] += b.m[dr][i] * m[i][dc];
				}
			}
		}

		return dest;
	}

	void multiplyVec4(const T* vIn, T* vOut)
	{
		vOut[0] = vIn[0] * m[0][0] + vIn[1] * m[1][0] + vIn[2] * m[2][0] + vIn[3] * m[3][0];
		vOut[1] = vIn[0] * m[0][1] + vIn[1] * m[1][1] + vIn[2] * m[2][1] + vIn[3] * m[3][1];
		vOut[2] = vIn[0] * m[0][2] + vIn[1] * m[1][2] + vIn[2] * m[2][2] + vIn[3] * m[3][2];
		vOut[3] = vIn[0] * m[0][3] + vIn[1] * m[1][3] + vIn[2] * m[2][3] + vIn[3] * m[3][3];
	}

	void multiplyPos3(const T* vIn, T* vOut)
	{
		vOut[0] = vIn[0] * m[0][0] + vIn[1] * m[1][0] + vIn[2] * m[2][0] + m[3][0];
		vOut[1] = vIn[0] * m[0][1] + vIn[1] * m[1][1] + vIn[2] * m[2][1] + m[3][1];
		vOut[2] = vIn[0] * m[0][2] + vIn[1] * m[1][2] + vIn[2] * m[2][2] + m[3][2];
	}

	void multiplyDir3(const T* vIn, T* vOut)
	{
		vOut[0] = vIn[0] * m[0][0] + vIn[1] * m[1][0] + vIn[2] * m[2][0];
		vOut[1] = vIn[0] * m[0][1] + vIn[1] * m[1][1] + vIn[2] * m[2][1];
		vOut[2] = vIn[0] * m[0][2] + vIn[1] * m[1][2] + vIn[2] * m[2][2];
	}

	void toFlat(T* vOut)
	{
		size_t i = 0;
		for (auto r = 0; r < 4; r += 1) {
			for (auto c = 0; c < 4; c += 1) {
				vOut[i] = m[r][c];
				i += 1;
			}
		}
	}
};

//-----------------------------------------------------------------------------
// Shaders
//-----------------------------------------------------------------------------

const char* vertex_shader = R""""(
	#version 110
	
	uniform mat4 matrix;

	attribute vec3 position;
	attribute vec4 colour;

	varying vec4 vColour;

	void main()
	{
		gl_Position = matrix * vec4(position, 1);
		vColour = colour;
	}
)"""";

const char* fragment_shader = R""""(
	#version 110
	
	varying vec4 vColour;

	void main()
	{
		gl_FragColor = vColour;
	}
)"""";

class Shader {
public:
	GLuint fragment, vertex, program;
	
	GLuint position;
	GLuint colour;
	
	GLuint matrix;
	
	void init()
	{
		fragment = compileShader(fragment_shader, GL_FRAGMENT_SHADER);
		vertex = compileShader(vertex_shader, GL_VERTEX_SHADER);
		program = linkProgram(vertex, fragment);
		
		matrix = glGetUniformLocation(program, "matrix");
		position = glGetAttribLocation(program, "position");
		colour = glGetAttribLocation(program, "colour");
	}
	
private:
	GLuint linkProgram(GLuint vert, GLuint frag)
	{
		GLuint prog = glCreateProgram();
		glAttachShader(prog, vert);
		glAttachShader(prog, frag);
		glLinkProgram(prog);
		
		GLint programOK;
		glGetProgramiv(prog, GL_LINK_STATUS, &programOK);
		if (programOK != GL_TRUE)
		{
			GLsizei logSize;
			glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &logSize);
			char* log = new char[logSize];
			glGetProgramInfoLog(prog, logSize, nullptr, log);
			std::cout << "Failed to link program: '" << log << "'" << std::endl;
			delete[] log;
			
			glDeleteProgram(prog);
			glDeleteShader(vert);
			glDeleteShader(frag);
		}
		
		return prog;
	}

	GLuint compileShader(const char* source, GLuint shaderType)
	{
		GLuint shader = glCreateShader(shaderType);
		glShaderSource(shader, 1, &source, nullptr);
		glCompileShader(shader);
		
		GLint shaderOK;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &shaderOK);
		if (shaderOK != GL_TRUE)
		{
			GLsizei logSize;
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logSize);
			char* log = new char[logSize];
			glGetShaderInfoLog(shader, logSize, nullptr, log);
			std::cout << "Failed to compile shader: '" << log << "'" << std::endl;
			delete[] log;
			
			glDeleteShader(shader);
		}
		
		return shader;
	}
};

//-----------------------------------------------------------------------------
// Renderer
//-----------------------------------------------------------------------------

struct Vertex
{
	float position[3];
	float colour[4];
};

class Renderer
{
private:
	Shader shader;
	float viewportSize[2];

public:
	void init()
	{
		shader.init();
	}
	
	void setViewport(int x, int y, int w, int h)
	{
		viewportSize[0] = (float)w;
		viewportSize[1] = (float)h;
		
		glViewport(x, y, w, h);
		glClearColor(0.0, 0.0, 0.0, 1.0);
		glClearDepth(0.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_GEQUAL);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		glUseProgram(shader.program);
	}
	
	void setCamera(Orientation<float> &ori, float fov = 1.0f)
	{
		Matrix<float> cameraMatrix = Matrix<float>::camera(ori.forward, ori.right, ori.up, ori.position);
		// View matrix maps from camera space into OpenGL homogeneous coordinates:
		// +x is right, +y is up, -z is forward
		Matrix<float> swizzleMatrix = {
			{
				{  0,  0, -1,  0 }, // forward
				{  1,  0,  0,  0 }, // right
				{  0,  1,  0,  0 }, // up
				{  0,  0,  0,  1 },
				// x,  y,  z,  w
			}
		};
		
		float aspect = viewportSize[0] / viewportSize[1];
		Matrix<float> perspectiveMatrix = Matrix<float>::reversePerspective(fov, fov / aspect, 1, 4096);
		Matrix<float> combinedMatrix = perspectiveMatrix.multiply(swizzleMatrix).multiply(cameraMatrix);
		
		float flatMatrix[16];
		combinedMatrix.toFlat(flatMatrix);
		glUniformMatrix4fv(shader.matrix, 1, GL_FALSE, flatMatrix);
		
		glEnable(GL_DEPTH_TEST);
	}
	
	void drawMesh(const Vertex* verts, int numVerts, GLint meshType = GL_TRIANGLES)
	{
		if (numVerts == 0)
		{
			return;
		}
		
		glVertexAttribPointer(
			shader.position,
			4,
			GL_FLOAT,
			GL_FALSE,
			sizeof(Vertex),
			(void*)&(verts[0].position[0])
		);
		glEnableVertexAttribArray(shader.position);

		glVertexAttribPointer(
			shader.colour,
			4,
			GL_FLOAT,
			GL_FALSE,
			sizeof(Vertex),
			(void*)&(verts[0].colour[0])
		);
		glEnableVertexAttribArray(shader.colour);
		
		glDrawArrays(meshType, 0, numVerts);
		
		glDisableVertexAttribArray(shader.position);
		glDisableVertexAttribArray(shader.colour);
	}
};
