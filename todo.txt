TODO:
- DONE Global setup of terrain types.
- DONE Update terrain/procedural/ProceduralTerrainSource to use the global terrain types.
- DONE See how much bordering stuff we can remove in ProceduralTerrainSource,
  given that we want our returned data to be fully accurate.
  * The refactor removed much border stuff.
- DONE Remove TerrainChunk allocation from ProceduralTerrainSource::GetPiece,
  make it write to an existing one instead.
  * Removed by refactor.
- DONE Remove unused files.
- DONE Update all the copyright headers.
- WONTDO Remove unused code from used files.
  * The only place this is the case is in the library stuff, which can wait for a while.
- DONE Move documentation for grammar to something like USERMANUAL.md.
- WONTDO Make flattening the water boundary an option.
  * This is more of a post-processing option to do elsewhere.
  * It still might be worth making a utility function, but we don't want our
    initial generators to do this.
- DONE Complete USERMANUAL.md:
  - DONE General concepts.
  - DONE The interface and how to use it.
- DONE Tidy up the appendix in USERMANUAL.md.
- DONE Create skeleton test app.
- DONE Fix bug where the target height buffer is added to rather than set.
- DONE Fix bug in test where it doesn't generate properly.
- DONE Fix bug in test triggered by the generated area not lining up to the map grid.
- Create various test cases and valgrind them.
  * For now just something that gets all the code paths working.
  * Things to test (all doable with a single set of terrain types):
    * Single terrain type.
    * Multi terrain type with generated map.
    * All parts of the terrain grammar.
  - DONE Render various tiles of terrain and output to file for inspection. (maybe tga because it's so simple).
  - DONE Render tiles with different offsets and ensure that the overlapping parts have the same values.
  - DONE Render aligned to map cells, and also misaligned spanning multiple of them.
  - DONE Check that the terrain map is called.
- DONE Refactor files to make sense in current context (change names and folders).
- DONE Fix dynamic linkage with test.
- DONE A "Just-flippin-render-it" library for OpenGL, to kind of recreate GL1.1 type interface.
- DONE Visualisation app for terrain types, maybe at least supporting live reloading.
  It could possibly evolve into a full map editor.
  Should it be part of this project?
  * It'll require some extra dependencies.
  * It's handy to have a reference usage as part of the project though.
- DONE Visualisation app improvements: Live reload of terrain
- DONE Figure out a way to do LOD, i.e. generating a lower LOD of heights.
  * It looks like there's already a scale parameter in the code, so it might
    be a case of setting up a test for it and hooking it up in the API.
    We'll want to check the coordinates.
  - DONE Pass LOD parameter through.
  - DONE Write test for it.
  - DONE Test with multi.
  - DONE Review coordinate system and see if it makes sense.
    * I think we should pass the regular tile coordinates, but expect a smaller buffer.
      Requiring different coordinates for LOD seems unintuitive.
      Or, does it make sense because we divide the whole lot, x, y, w, h, by LOD?
      I think we need to understand what LOD actually means technically.
      It's possible to pass non-multiple-of-lod x and y coords, and these don't seem to line up to anything.
      Thus we'll pass the regular coords but expect a smaller buffer.
  - DONE Update readme docs with new parameters and explain LOD.
- DONE Review wording of documentation (not quite happy with LOD).
  * It seems alright.
- DONE Input validation (things like LOD, and check for other asserts in the code).
  - DONE Initialise grammar error.
  - DONE Unit tests with invalid values.
    - DONE Bad script failure tests.
    - DONE LOD & map scale
    - DONE Width & height
  - DONE Implement validation to fix tests.
- DONE Reload a terrain type in-place (keeping index), but keeping the original if the new one fails.
  (Handy for world editors)
- DONE (GPL is better) Review license to see if LGPL is better.

- Look at other places we can remove allocations, including the generation buffers.
- Clarify a lot of variable naming in ProceduralTerrainSource, and tidy code up.
- Make tile type alignment be an option. (Vertex vs cell).
  * This requires doing the texturing calculations in a slightly different way.
- Option to skip clearing the output buffers for when the user knows that they are zero.
- Visualisation app improvements
  - Show name of file being rendered
  - On-screen instructions
  - Move around the terrain, reset to centre, show larger terrain area
  - Use Vulkan instead of OpenGL, or find/make a portable mini-OpenGL wrapper, or some utility library.
- Investigate using a separable filter instead of optimized Lanczos.
- Investigate kernel for equilateral triangles.
- Consider adding shoreline flattening and 'nicest' diagonal calculation.
- Handle LOD where we're rendering smaller than mapscale.
