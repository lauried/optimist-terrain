# Optimist Terrain Generation

## Building

### Linux

The general build process is as follows:

```
cmake -S source -B build
cd build
make
cd ..
```

For linux, there are some shell scripts to automate this.
The libraries and tests can be built with:

```
./cmake.sh
./make.sh
```

The viewer application can be built with:

```
./viewer_make.sh
```

It requires the development packages for SDL2, OpenGL and GLEW.

### Other

I don't know what you'll have to do after `cmake -S source -B build`.
